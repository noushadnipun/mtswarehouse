<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PpiSpi extends Model
{
    use HasFactory;
    protected $table = 'ppi_spis';
    protected $fillable = [
        'action_format', 'ppi_spi_type', 'project', 'tran_type', 'note','warehouse_id', 'action_performed_by' , 'transferable'
    ];


    public function stock(){
        return $this->hasMany('\App\Models\ProductStock','ppi_spi_id', 'id');
    }
    public function source(){
        return $this->hasMany('\App\Models\PpiSpiSource','ppi_spi_id', 'id');
    }
}
