<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Warehouse\PpiController;
use App\Models\PpiSpi;
use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models\Warehouse;
use App\Models\Roleuser;
use App\Models\PpiSpiNotification;
use Carbon\Carbon;

class WarehouseController extends Controller
{
    private $model;
    private $roleuser;

    public function __construct(Warehouse $model, Roleuser $roleuser)
    {
        $this->model = $model;
        $this->roleuser = $roleuser;
    }

    /**
     * @index
     */
    public function index()
    {
        //$wh = $this->model::get();
        $wh = auth()->user()->getUserWarehouse();
        return view('admin.pages.warehouse.index', compact('wh'));
    }

    /**
     * @Create
     */
    public function create()
    {
        return view('admin.pages.warehouse.form');
    }

    /**
     * Data insert to DB
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $validator = Validator::make($request->all(),
            [
                'name' => 'required',
            ]
        );
        // process the login
        if ($validator->fails()) {
            return redirect('warehouse.create')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $hash = bin2hex(random_bytes(2));
            $attributes = [
                'name' => $request->name,
                'code' => strtolower(str_replace(' ', '_', $request->name)) . '_' . $hash,
                'email' => $request->email,
                'phone' => $request->phone,
                'location' => $request->location,
            ];
            //dd($attributes);
            $warehouse = $this->model::create($attributes);

            //Insert roleuser table
            $assignAttr = [];
            if ($request->assign_user != null) {
                foreach ($request->assign_user as $key => $assignUser) {
                    $assignAttr [] = $attributes = [
                        'user_id' => $assignUser['user_id'],
                        'role_id' => $assignUser['role_id'],
                        'warehouse_id' => $warehouse->id,
                    ];
                }//End Foreach
                //dd($assignAttr);
                $whr = $this->roleuser::insert($assignAttr);
            }//End if

            try {
                return redirect()->route('warehouse_index')->with(['status' => 1, 'message' => 'Successfully created user']);
            } catch (\Exception $e) {
                //dd($e->errorInfo[2]);
                $errormsg = $e->errorInfo[2];
            }
        }
    }

    /**
     * Edit a Single Warehouse
     * by Id
     */
    public function edit($id)
    {
        $wh = $this->model::find($id);
        $assignedUser = $this->roleuser::where('warehouse_id', $id)->get();
        return view('admin.pages.warehouse.form', compact('wh', 'assignedUser'));
    }

    /**
     * Update Data of DB
     *
     */
    public function update(Request $request)
    {
        //Catch Warehouse Information
        $attributes = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'location' => $request->location,
        ];
        $wh = $this->model::where('id', $request->id)->update($attributes); //End Warehouse Inormation Update

        //Assign User to Warehouse
        $existing = $this->roleuser::where('warehouse_id', $request->id)->delete() ?? Null;//Delete Existing Data

        //Store to RoleUser
        $assignAttr = [];
        if ($request->assign_user != null) {
            foreach ($request->assign_user as $key => $assignUser) {
                $assignAttr [] = $attributes = [
                    'user_id' => $assignUser['user_id'],
                    'role_id' => $assignUser['role_id'],
                    'warehouse_id' => $request->id,
                ];
            }//End Foreach
            //dd($assignAttr);
            $whr = $this->roleuser::insert($assignAttr);
        }//End if

        try {
            return redirect()->back()->with(['status' => 1, 'message' => 'Successfully updated']);
        } catch (\Exception $e) {
            return redirect()->back()->with(['status' => 0, 'message' => 'Error']);
        }
    }

    /**
     * Delete Data from DB
     */
    public function destroy($id)
    {
        $wh = $this->model::find($id);
        $wh->delete();
        return redirect()->back()->with(['status' => 1, 'message' => 'Successfully deleted']);
    }


    public function notofication($status_id)
    {
        $cn = $this->Model('PpiSpiNotification')::where('status_id', $status_id)->first();
        $status = $this->Model('PpiSpiStatus')::where('id', $status_id)->first();
        $warehouse_code = $this->Model('Warehouse')::getColumn($status->warehouse_id, 'code');

        if (!empty($cn) && ($cn->is_read == 1 || $cn->is_read == 0)) {

        } else {
            $attr = [
                'status_id' => $status_id,
                'is_read' => 1,
                'action_performed_by' => auth()->user()->id,
            ];
            PpiSpiNotification::create($attr);
        }


        if ($status->status_for == 'Ppi') {
            return redirect(route('ppi_edit', [$warehouse_code, $status->ppi_spi_id]));
        }
        if ($status->status_for == 'Spi') {
            return redirect()->route('spi_edit', [$warehouse_code, $status->ppi_spi_id]);
        }
        return null;
    }

    /**
     * @param Request $request
     * @return void
     * Clear All Notification
     */

    public function notoficationClearAll(Request $request)
    {
//        dd($request->all());
        $exId = explode('|', $request->clearAll);
//        dd($exId);
        $upDateId = null;
        $attr = null;
        foreach ($exId as $id) {
            $check = PpiSpiNotification::where('status_id', $id)->first();
            if ($check) {
                $upDateId [] = $check->id;
            } else {
                $attr [] = [
                    'status_id' => $id,
                    'is_read' => 2,
                    'action_performed_by' => auth()->user()->id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
            }
        }
        if (!empty($upDateId)) {
            PpiSpiNotification::whereIn('id', $upDateId)->update(['is_read' => 2]);
        }
        if (!empty($attr)) {
            PpiSpiNotification::insert($attr);
        }
        return redirect()->back();
    }


    /**
     *=================
     *=== Report ======
     *=================
     */

    public function productStock(Request $request)
    {
        if ($request->product_id) {
            $product = $this->Model('ProductStockReport')::find($request->product_id);
            return view('admin.pages.warehouse.report.product-stock-details')->with(['product' => $product]);
        } else {
            return view('admin.pages.warehouse.report.product-stock');
        }
    }

    public function apiGetProductStock(Request $request)
    {

        $query = $this->Model('ProductStockReport')::query();
        /** Filed Show for loop */
        $phpCode = '
            $link = route("report_product_stock_details", $data->id);
        ';
        $fields = [
            'id' => '$data->id',
            'name' => '"<a target=\"_blank\" class=\"text-primary\" href=\"{$link}\">{$data->name}</a>"',
            'code' => '$data->code',
            'stock_in' => '$data->stock_in',
            'waiting_stock_in' => '$data->waiting_stockin',
            'stock_out' => '$data->stock_out',
            'waiting_stock_out' => '$data->waiting_stockout',
            'stock_in_hand' => '$data->stock_in_hand',
            'unit' => '$this->Model("AttributeValue")::getValueById($data->unit_id)',
        ];
        return $this->Datatable::generate($request, $query, $fields, ['orderby' => 'asc', 'phpcode' => $phpCode]);
    }
}
