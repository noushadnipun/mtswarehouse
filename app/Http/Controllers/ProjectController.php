<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use Validator;
class ProjectController extends Controller
{    

    protected $model;     
    /**
     * __construct
     *
     * @param  mixed $model
     * @return void
     */
    public function __construct(Project $model){
        $this->model = $model;
    }
    /**
     * index
     *
     * @return void
     */
    public function index(){
        return view ('admin.pages.warehouse.project.index');
    }
    
    /**
     * create
     *
     * @return void
     */
    public function create(){

    }
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else {
            $attributes = [
                'name' => $request->name,
                'code' => strtolower(str_replace(' ', '_', $request->name)),
                'type' => $request->type,
                'customer' => $request->customer,
                'vendor' => $request->vendor,
                'note' => $request->note,
            ];
            try {
                $data = $this->model::create($attributes);
                return redirect()->back()->with(['status' => 1, 'message' => 'Successfully created']);
            } catch (\Exception $e) {
                return redirect()->back();
            }
        }   
    }
    
    /**
     * edit
     *
     * @param  mixed $id
     * @return void
     */
    public function edit($id){
        $project = $this->model::find($id);
        return view('admin.pages.warehouse.project.index', ['project' => $project]);
    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @return void
     */
    public function update(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else {
            $attributes = [
                'name' => $request->name,
                'code' => strtolower(str_replace(' ', '_', $request->name)),
                'type' => $request->type,
                'customer' => $request->customer,
                'vendor' => $request->vendor,
                'note' => $request->note,
            ];
            try {
                $data = $this->model::where('id', $request->id)->update($attributes);
                return redirect()->back()->with(['status' => 1, 'message' => 'Successfully created']);
            } catch (\Exception $e) {
                return redirect()->back();
            }
        }   
    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id){
        
    }
}
