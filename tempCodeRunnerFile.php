<?php

namespace App\Http\Controller\Admin;
use App\Models\Spi;

/**
 * SpiController
 */
class SpiController extends Controller{

    /**
     * model
     *
     * @var mixed
     */
    private $model;
    /**
     * __construct
     *
     * @param  mixed $model
     * @return void
     */
    public function  __construct(Spi $model){
        $this->model = $model;
    }

    /**
     * index
     *
     * @return void
     */
    public function index(){
        retun view('admin.pages.warehouse.spi.index');
    }

    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        # code...
    }

    /**
     * edit
     *
     * @param  mixed $id
     * @return void
     */
    public function edit($id)
    {
        # code...
    }

    /**
     * update
     *
     * @param  mixed $request
     * @return void
     */
    public function update(Request $request)
    {
        # code...
    }

    /**
     * destroy
     *
     * @param  mixed $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $data = $this->model::find($id);
        $done = $data->delete();
        if($done){
            return redirect()->back()->with([
                'status' => '1',
                'message' => 'Successfully deleted.'
            ]);
        }

    }
}
