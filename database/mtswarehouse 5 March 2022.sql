-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 05, 2022 at 04:04 PM
-- Server version: 8.0.20
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mtswarehouse`
--

-- --------------------------------------------------------

--
-- Table structure for table `attribute_values`
--

CREATE TABLE `attribute_values` (
  `id` bigint UNSIGNED NOT NULL,
  `unique_name` enum('Unit','Brand','Contact Type') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_values`
--

INSERT INTO `attribute_values` (`id`, `unique_name`, `value`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(51, 'Unit', 'Mtr', 'mtr', 'Active', '2022-01-30 09:29:37', '2022-01-30 09:29:37'),
(52, 'Unit', 'Pcs', 'pcs', 'Active', '2022-01-30 09:29:37', '2022-01-30 09:29:37'),
(53, 'Contact Type', 'Customer', 'customer', 'Active', NULL, NULL),
(54, 'Contact Type', 'Supplier', 'supplier', 'Active', NULL, NULL),
(55, 'Contact Type', 'Sub Supllier', 'sub-supplier', 'Active', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `license_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_person` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_person_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat_type` enum('Inclusive','Exclusive') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat_percent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_type` enum('Inclusive','Exclusive') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_percent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `phone`, `email`, `address`, `license_no`, `contact_person`, `contact_person_no`, `vat_type`, `vat_percent`, `tax_type`, `tax_percent`, `note`, `created_at`, `updated_at`) VALUES
(3, 'BL', '019000', 'info@bl.com', 'Gulshan', '00000', 'BL Admin', '019899', 'Exclusive', '7.1', 'Exclusive', '2', 'nai', '2021-09-13 13:26:40', '2021-09-13 13:27:25'),
(4, 'GP', '017', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-15 23:31:14', '2021-09-15 23:31:14'),
(5, 'Robi', '018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-15 23:31:32', '2021-09-15 23:31:32'),
(6, 'UDC', '010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-15 23:31:42', '2021-09-15 23:31:42'),
(7, 'MTSBD', '01800', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-15 23:32:44', '2021-09-15 23:32:44'),
(8, 'Sunrise Electric', '01823633792', 'sunrise@mail.com', 'Nobabpur', '0', 'Mr Sunrise', '01677618199', NULL, NULL, NULL, NULL, 'n/a', '2021-09-22 00:54:14', '2021-09-22 00:54:14');

-- --------------------------------------------------------

--
-- Table structure for table `global_settings`
--

CREATE TABLE `global_settings` (
  `id` bigint UNSIGNED NOT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_type` enum('Text','Textarea','Select','Richeditor','Number','Checkbox') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_group` enum('General','Homepage','Header Section') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_order` int DEFAULT NULL,
  `meta_placeholder` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `global_settings`
--

INSERT INTO `global_settings` (`id`, `meta_title`, `meta_name`, `meta_value`, `meta_type`, `meta_group`, `meta_order`, `meta_placeholder`, `created_at`, `updated_at`) VALUES
(1, 'PPI Auto Approve Boss', 'ppi_auto_approve_boss', NULL, 'Checkbox', NULL, NULL, NULL, NULL, '2021-11-11 05:59:26'),
(2, 'Boss User ID', 'boos_user_id', '23', 'Text', NULL, NULL, 'Type Boss user ID', NULL, '2021-11-09 12:12:41');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2021_07_14_054017_create_roles_table', 1),
(4, '2021_07_14_054543_create_warehouses_table', 1),
(5, '2021_07_14_054544_create_role_users_table', 1),
(11, '2021_07_14_061139_create_route_groups_table', 2),
(12, '2021_07_14_061140_create_route_lists_table', 2),
(13, '2021_07_14_061655_create_route_list_roles_table', 2),
(15, '2021_08_31_063743_create_attribute_values_table', 3),
(16, '2021_08_03_055209_create_products_table', 4),
(17, '2021_09_02_123624_create_product_categories_table', 5),
(19, '2021_09_05_100553_create_projects_table', 6),
(20, '2021_09_13_182654_create_contacts_table', 7),
(22, '2021_09_03_105611_create_ppi_spis_table', 8),
(23, '2021_09_18_045738_create_ppi_products_table', 8),
(24, '2021_09_22_215046_create_ppi_set_products_table', 9),
(26, '2021_09_26_061805_create_ppi_spi_statuses_table', 10),
(28, '2021_09_30_165636_create_ppi_spi_disputes_table', 11),
(30, '2021_11_04_131609_create_ppi_bundle_products_table', 12),
(31, '2021_11_06_184545_create_global_settings_table', 13),
(34, '2022_01_15_174005_create_product_stocks_table', 14),
(35, '2022_02_09_115217_create_ppi_spi_sources_table', 15),
(37, '2022_02_10_223437_create_spi_products_table', 16);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ppi_bundle_products`
--

CREATE TABLE `ppi_bundle_products` (
  `id` bigint UNSIGNED NOT NULL,
  `ppi_id` bigint UNSIGNED NOT NULL,
  `ppi_product_id` bigint UNSIGNED NOT NULL,
  `product_id` bigint UNSIGNED DEFAULT NULL,
  `warehouse_id` bigint UNSIGNED NOT NULL,
  `bundle_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bundle_size` int NOT NULL,
  `bundle_price` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `action_performed_by` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ppi_bundle_products`
--

INSERT INTO `ppi_bundle_products` (`id`, `ppi_id`, `ppi_product_id`, `product_id`, `warehouse_id`, `bundle_name`, `bundle_size`, `bundle_price`, `action_performed_by`, `created_at`, `updated_at`) VALUES
(44, 10, 29, 1, 21, '129_10', 10, '27', 18, '2022-03-05 09:28:57', '2022-03-05 09:28:57'),
(45, 10, 29, 1, 21, '129_15', 15, '17', 18, '2022-03-05 09:28:57', '2022-03-05 09:28:57'),
(46, 10, 29, 1, 21, '129_30', 30, '17', 18, '2022-03-05 09:28:57', '2022-03-05 09:28:57');

-- --------------------------------------------------------

--
-- Table structure for table `ppi_products`
--

CREATE TABLE `ppi_products` (
  `id` bigint UNSIGNED NOT NULL,
  `ppi_id` bigint UNSIGNED NOT NULL,
  `warehouse_id` bigint UNSIGNED NOT NULL,
  `product_id` bigint UNSIGNED NOT NULL,
  `qty` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit_price` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `price` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `product_state` enum('New','Used','Cut-Piece') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `health_status` enum('Useable','Scrapped') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_performed_by` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ppi_products`
--

INSERT INTO `ppi_products` (`id`, `ppi_id`, `warehouse_id`, `product_id`, `qty`, `unit_price`, `price`, `product_state`, `health_status`, `note`, `action_performed_by`, `created_at`, `updated_at`) VALUES
(25, 10, 21, 32, '11', '350', '3850', 'New', 'Useable', NULL, 23, '2022-03-05 09:15:00', '2022-03-05 11:37:55'),
(26, 10, 21, 6, '5', '17', '85', 'New', 'Useable', NULL, 18, '2022-03-05 09:15:35', '2022-03-05 09:15:35'),
(27, 10, 21, 20, '3', '30', '90', 'Used', 'Useable', NULL, 18, '2022-03-05 09:18:00', '2022-03-05 09:18:00'),
(28, 10, 21, 33, '5', '50', '250', 'New', 'Useable', NULL, 18, '2022-03-05 09:18:00', '2022-03-05 09:18:00'),
(29, 10, 21, 1, '0', '0', '1035', 'Cut-Piece', 'Useable', NULL, 18, '2022-03-05 09:28:57', '2022-03-05 09:28:57'),
(30, 10, 21, 37, '40', '100', '4000', 'New', 'Useable', NULL, 18, '2022-03-05 09:28:57', '2022-03-05 09:28:57'),
(31, 10, 21, 56, '20', '250', '5000', 'New', 'Useable', NULL, 18, '2022-03-05 09:28:57', '2022-03-05 09:28:57');

-- --------------------------------------------------------

--
-- Table structure for table `ppi_set_products`
--

CREATE TABLE `ppi_set_products` (
  `id` bigint UNSIGNED NOT NULL,
  `ppi_id` bigint UNSIGNED NOT NULL,
  `warehouse_id` bigint UNSIGNED NOT NULL,
  `set_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ppi_product_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action_performed_by` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ppi_set_products`
--

INSERT INTO `ppi_set_products` (`id`, `ppi_id`, `warehouse_id`, `set_name`, `ppi_product_id`, `action_performed_by`, `created_at`, `updated_at`) VALUES
(2, 10, 21, 'SET ONE', '25,26', 18, '2022-03-05 09:17:13', '2022-03-05 09:17:13'),
(3, 10, 21, 'SET TWO', '27,28', 18, '2022-03-05 09:18:13', '2022-03-05 09:18:13');

-- --------------------------------------------------------

--
-- Table structure for table `ppi_spis`
--

CREATE TABLE `ppi_spis` (
  `id` bigint UNSIGNED NOT NULL,
  `action_format` enum('Ppi','Spi') COLLATE utf8mb4_unicode_ci NOT NULL,
  `ppi_spi_type` enum('Supply','Service','Other') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `project` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tran_type` enum('With Money','Without Money','Other') COLLATE utf8mb4_unicode_ci NOT NULL,
  `source_tree` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `warehouse_id` bigint UNSIGNED NOT NULL,
  `action_performed_by` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ppi_spis`
--

INSERT INTO `ppi_spis` (`id`, `action_format`, `ppi_spi_type`, `project`, `tran_type`, `source_tree`, `warehouse_id`, `action_performed_by`, `created_at`, `updated_at`) VALUES
(10, 'Ppi', 'Supply', 'Project 1', 'With Money', NULL, 21, 18, '2022-03-05 09:14:31', '2022-03-05 09:14:31');

-- --------------------------------------------------------

--
-- Table structure for table `ppi_spi_disputes`
--

CREATE TABLE `ppi_spi_disputes` (
  `id` bigint UNSIGNED NOT NULL,
  `ppi_spi_status_id` bigint UNSIGNED NOT NULL,
  `ppi_spi_id` bigint UNSIGNED NOT NULL,
  `status_for` enum('Ppi','Spi') COLLATE utf8mb4_unicode_ci NOT NULL,
  `ppi_spi_product_id` bigint UNSIGNED NOT NULL,
  `issue_column` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `warehouse_id` bigint UNSIGNED NOT NULL,
  `action_performed_by` bigint UNSIGNED NOT NULL,
  `action_format` enum('Dispute','Correction') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `correction_dispute_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ppi_spi_disputes`
--

INSERT INTO `ppi_spi_disputes` (`id`, `ppi_spi_status_id`, `ppi_spi_id`, `status_for`, `ppi_spi_product_id`, `issue_column`, `note`, `warehouse_id`, `action_performed_by`, `action_format`, `correction_dispute_id`, `created_at`, `updated_at`) VALUES
(47, 193, 10, 'Ppi', 25, 'product', 'wrong product', 21, 24, 'Dispute', NULL, '2022-03-05 09:56:59', '2022-03-05 09:56:59'),
(48, 195, 10, 'Ppi', 25, NULL, NULL, 21, 23, 'Correction', '47', '2022-03-05 11:41:02', '2022-03-05 11:41:02');

-- --------------------------------------------------------

--
-- Table structure for table `ppi_spi_sources`
--

CREATE TABLE `ppi_spi_sources` (
  `id` bigint UNSIGNED NOT NULL,
  `ppi_spi_id` bigint UNSIGNED DEFAULT NULL,
  `action_format` enum('Ppi','Spi') COLLATE utf8mb4_unicode_ci NOT NULL,
  `source_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `who_source` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `levels` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `warehouse_id` bigint UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ppi_spi_sources`
--

INSERT INTO `ppi_spi_sources` (`id`, `ppi_spi_id`, `action_format`, `source_type`, `who_source`, `levels`, `warehouse_id`, `created_at`, `updated_at`) VALUES
(19, 10, 'Ppi', 'Customer', 'BL', '2', 21, '2022-03-05 09:14:31', '2022-03-05 09:14:31'),
(20, 10, 'Ppi', 'Site', 'X90086', '2', 21, '2022-03-05 09:14:31', '2022-03-05 09:14:31');

-- --------------------------------------------------------

--
-- Table structure for table `ppi_spi_statuses`
--

CREATE TABLE `ppi_spi_statuses` (
  `id` bigint UNSIGNED NOT NULL,
  `ppi_spi_id` bigint UNSIGNED NOT NULL,
  `status_for` enum('Ppi','Spi') COLLATE utf8mb4_unicode_ci NOT NULL,
  `warehouse_id` bigint UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action_performed_by` bigint UNSIGNED NOT NULL,
  `status_order` int DEFAULT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_type` enum('success','danger','warning','info','purple') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'success',
  `status_format` enum('Main','Optional') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ppi_spi_product_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ppi_spi_statuses`
--

INSERT INTO `ppi_spi_statuses` (`id`, `ppi_spi_id`, `status_for`, `warehouse_id`, `code`, `action_performed_by`, `status_order`, `message`, `status_type`, `status_format`, `note`, `ppi_spi_product_id`, `created_at`, `updated_at`) VALUES
(181, 10, 'Ppi', 21, 'ppi_created', 18, 1, 'Ppi created', 'success', 'Main', NULL, NULL, '2022-03-05 09:14:31', '2022-03-05 09:14:31'),
(182, 10, 'Ppi', 21, 'ppi_product_added', 18, 2, 'Product Added in PPI', 'success', 'Optional', 'Product: Omni Antenna', NULL, '2022-03-05 09:15:00', '2022-03-05 09:15:00'),
(183, 10, 'Ppi', 21, 'ppi_product_added', 18, 3, 'Product Added in PPI', 'success', 'Optional', 'Product: 1/2 inch DIN Superflex Male Connector', NULL, '2022-03-05 09:15:35', '2022-03-05 09:15:35'),
(184, 10, 'Ppi', 21, 'ppi_set_created', 18, 4, 'Set Created in PPI', 'success', 'Optional', NULL, NULL, '2022-03-05 09:17:13', '2022-03-05 09:17:13'),
(185, 10, 'Ppi', 21, 'ppi_product_added', 18, 5, 'Product Added in PPI', 'success', 'Optional', 'Product: 2-meter Jumper  I+I', NULL, '2022-03-05 09:18:00', '2022-03-05 09:18:00'),
(186, 10, 'Ppi', 21, 'ppi_product_added', 18, 6, 'Product Added in PPI', 'success', 'Optional', 'Product: 3-Way Spliter', NULL, '2022-03-05 09:18:00', '2022-03-05 09:18:00'),
(187, 10, 'Ppi', 21, 'ppi_set_created', 18, 7, 'Set Created in PPI', 'success', 'Optional', NULL, NULL, '2022-03-05 09:18:13', '2022-03-05 09:18:13'),
(188, 10, 'Ppi', 21, 'ppi_product_added', 18, 8, 'Product Added in PPI', 'success', 'Optional', 'Product: 7/8 Feeder Cable', NULL, '2022-03-05 09:28:57', '2022-03-05 09:28:57'),
(189, 10, 'Ppi', 21, 'ppi_product_added', 18, 9, 'Product Added in PPI', 'success', 'Optional', 'Product: PM3 Card', NULL, '2022-03-05 09:28:57', '2022-03-05 09:28:57'),
(190, 10, 'Ppi', 21, 'ppi_product_added', 18, 10, 'Product Added in PPI', 'success', 'Optional', 'Product: MRFU_1800', NULL, '2022-03-05 09:28:57', '2022-03-05 09:28:57'),
(191, 10, 'Ppi', 21, 'ppi_sent_to_boss', 18, 11, 'Ppi sent to Boss', 'success', 'Main', NULL, NULL, '2022-03-05 09:29:23', '2022-03-05 09:29:23'),
(192, 10, 'Ppi', 21, 'ppi_sent_to_wh_manager', 23, 12, 'Ppi sent to Warehouse Manager', 'success', 'Main', NULL, NULL, '2022-03-05 09:35:17', '2022-03-05 09:35:17'),
(193, 10, 'Ppi', 21, 'ppi_dispute_by_wh_manager', 24, 13, 'Dispute by Warehouse Manager', 'danger', 'Main', 'with Omni Antenna', '25', '2022-03-05 09:56:59', '2022-03-05 09:56:59'),
(194, 10, 'Ppi', 21, 'ppi_product_edited', 23, 14, 'Product Edited in PPI', 'purple', 'Optional', 'with Omni Antenna', '25', '2022-03-05 11:37:55', '2022-03-05 11:37:55'),
(195, 10, 'Ppi', 21, 'ppi_product_info_correction_by_boss', 23, 15, 'Ppi Product Infomation Correction by Boss', 'success', 'Optional', 'with Omni Antenna', '25', '2022-03-05 11:41:02', '2022-03-05 11:41:02'),
(196, 10, 'Ppi', 21, 'ppi_correction_done_by_boss', 23, 16, 'Ppi Correction Done by Boss', 'success', 'Main', NULL, NULL, '2022-03-05 11:41:02', '2022-03-05 11:41:02'),
(197, 10, 'Ppi', 21, 'ppi_resent_to_wh_manager', 23, 17, 'Ppi Resent to Warehouse Manager', 'success', 'Main', NULL, NULL, '2022-03-05 11:41:37', '2022-03-05 11:41:37');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` bigint NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `brand_id` int DEFAULT NULL,
  `unit_id` int DEFAULT NULL,
  `warehouse_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barcode_format` enum('Tag','Without-Tag') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barcode_prefix` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unique_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stock_qty_alert` int DEFAULT NULL,
  `category_id` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `user_id`, `name`, `code`, `slug`, `description`, `brand_id`, `unit_id`, `warehouse_id`, `product_type`, `barcode_format`, `barcode_prefix`, `unique_key`, `stock_qty_alert`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 18, '7/8 Feeder Cable', '7/8FCBL', NULL, NULL, NULL, 51, '21,24', 'Supply,Service', 'Without-Tag', NULL, 'MTS0fbf0', 20, 14, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(2, 18, '1/2 Flex Feeder Cable', '1/2FFCBL', NULL, NULL, NULL, 51, '21,24', 'Supply,Service', 'Without-Tag', NULL, 'MTS27ca1', 20, 14, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(3, 18, '1/2 Super Flex Feeder Cable', '1/2SFFCBL', NULL, NULL, NULL, 51, '21,24', 'Supply,Service', 'Without-Tag', NULL, 'MTS49e12', 20, 14, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(4, 18, 'CPRI 50 mtr LC-LC', 'CPRI50-50LCLC', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Without-Tag', NULL, 'MTS23a63', 20, 15, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(5, 18, '1/2 inch DIN Superflex Female Connector', '1/2DINSFFCNCTR', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS1/2DINSFFCNCTR', 'MTS3d794', 20, 16, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(6, 18, '1/2 inch DIN Superflex Male Connector', '1/2DINSMCNCTR', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS1/2DINSMCNCTR', 'MTS72bb5', 20, 16, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(7, 18, '1/2 inch DIN  Flex Male Connector', '1/2DINFMCNCTR', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS1/2DINFMCNCTR', 'MTS5e226', 20, 16, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(8, 18, '1/2 inch N  Flex Male Connector', '1/2NFMCNCTR', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS1/2NFMCNCTR', 'MTSe43a7', 20, 16, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(9, 18, '1/2 inch N  Flex Female Connector', '1/2NFFCNCTR', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS1/2NFFCNCTR', 'MTS811e8', 20, 16, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(10, 18, '1/2 inch DIN Male L Connector', '1/2DINMLCNCTR', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS1/2DINMLCNCTR', 'MTS2ed69', 20, 16, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(11, 18, '1/2 inch Ring Flare Connector', '1/2RINGFLRCNCTR', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS1/2RINGFLRCNCTR', 'MTS21b710', 20, 16, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(12, 18, '4.3-10 Connector', '4.3-10CNCTR', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS4.3-10CNCTR', 'MTS390c11', 20, 16, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(13, 18, '7/8 inch N Male Connector', '7/8NMCNCTR', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS7/8NMCNCTR', 'MTSb16612', 20, 16, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(14, 18, '7/8 inch N Female Connector', '7/8NFCNCTR', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS7/8NFCNCTR', 'MTS5b1613', 20, 16, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(15, 18, '7/8 inch DIN Male Connector', '7/8DINMCNCTR', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS7/8DINMCNCTR', 'MTSd0c414', 20, 16, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(16, 18, '7/8 inch DIN Female Connector', '7/8DINFCNCTR', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS7/8DINFCNCTR', 'MTSee8115', 20, 16, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(17, 18, 'RRU Power Connector', 'RRUPCNCTR', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTSRRUPCNCTR', 'MTS337b16', 20, 17, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(18, 18, '9F Connector for TN Link Ericsson Connector', '9FCNTRTNERICCNR', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS9FCNTRTNERICCNR', 'MTS85d717', 20, 16, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(19, 18, '1/2\" Male To 7/16\" Female Connector', '1/2\"To7/16\"FCNCTR', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS1/2\"To7/16\"FCNCTR', 'MTS65b918', 20, 16, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(20, 18, '2-meter Jumper  I+I', '2-mtrJmprII', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS2-mtrJmprII', 'MTS4cf019', 20, 18, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(21, 18, '3-meter Jumper I+I', '3-mtrJmprII', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS3-mtrJmprII', 'MTSb7dd20', 20, 18, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(22, 18, '5-meter Jumper I+I', '5-mtrJmprII', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS5-mtrJmprII', 'MTSa13b21', 20, 18, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(23, 18, '10-meter Jumper I+I', '10-mtrJmprII', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS10-mtrJmprII', 'MTS712222', 20, 18, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(24, 18, '1-meter microw Jumper I+L', '1-mtrmcrwJmprIL', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS1-mtrmcrwJmprIL', 'MTSf1ec23', 20, 18, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(25, 18, '1-meter microw Jumper I+I', '1-mtrmcrwJmprII', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS1-mtrmcrwJmprII', 'MTS2d7124', 20, 18, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(26, 18, '7/8 feeder Clamp', '7/8FDRCLMP', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Without-Tag', NULL, 'MTS285a25', 20, 19, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(27, 18, '7/8 Grounding Kit', '7/8KKIKT', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Without-Tag', NULL, 'MTS3c5c26', 20, 19, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(28, 18, 'DCDB Small Closed', 'DCDBSCLSD', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTSDCDBSCLSD', 'MTS549327', 20, 20, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(29, 18, 'DCDB Big Closed', 'DCDBBCLSD', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTSDCDBBCLSD', 'MTSfc0428', 20, 20, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(30, 18, '12 Core Fiber Optics Messanger Cable', '12CRFOMSNGRCBL', NULL, NULL, NULL, 51, '21,24', 'Supply,Service', 'Without-Tag', NULL, 'MTS595f29', 20, 15, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(31, 18, '4 Core Fiber Optics Messanger Cable', '4CRFOMSNGRCBL', NULL, NULL, NULL, 51, '21,24', 'Supply,Service', 'Without-Tag', NULL, 'MTSb97c30', 20, 15, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(32, 18, 'Omni Antenna', 'OmniANT', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTSOmniANT', 'MTS7d1a31', 20, 21, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(33, 18, '3-Way Spliter', '3WYSPLTTR', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS3WYSPLTTR', 'MTSf87332', 20, 19, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(34, 18, '4-Way Spliter', '4WYSPLTTR', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS4WYSPLTTR', 'MTSdfe033', 20, 19, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(35, 18, '7 db Directional', '7DBDRCTNL', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS7DBDRCTNL', 'MTS732134', 20, 21, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(36, 18, '50 ohm Dummy', '50OhmDMY', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS50OhmDMY', 'MTS6c4035', 20, 19, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(37, 18, 'PM3 Card', 'PM3CRD', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTSPM3CRD', 'MTSe82836', 20, 22, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(38, 18, 'PM3 PWR Cable', 'PM3CRDPWRCBL', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Without-Tag', NULL, 'MTSc84137', 20, 22, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(39, 18, '2-meter Jumper  I+L', '2-mtrJmprIL', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS2-mtrJmprIL', 'MTS832838', 20, 18, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(40, 18, '3-meter I+L', '3-meterIL', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS3-meterIL', 'MTSa4de39', 20, 18, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(41, 18, '5-meter I+L', '5-meterIL', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS5-meterIL', 'MTSd36f40', 20, 18, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(42, 18, '10-meter I+L', '10-meterIL', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTS10-meterIL', 'MTSa47341', 20, 18, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(43, 18, 'DCDB Small Open', 'DCDBSOPN', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTSDCDBSOPN', 'MTSeb2c42', 20, 20, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(44, 18, 'MRRU_1800 ', 'MRRU_1800', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTSMRRU_1800', 'MTSdf9243', 20, 22, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(45, 18, 'WRRU', 'WRRU', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTSWRRU', 'MTS56ae44', 20, 22, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(46, 18, 'WRRU_2100', 'WRRU_2100', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTSWRRU_2100', 'MTS0ead45', 20, 22, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(47, 18, 'Short jumper', 'Shortjumper', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTSShortjumper', 'MTS96d046', 20, 18, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(48, 18, 'WRFU_2100', 'WRFU_2100', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTSWRFU_2100', 'MTS52c847', 20, 22, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(49, 18, 'CPRI 50 mtr', 'CPRI50mtr', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Without-Tag', NULL, 'MTSa91748', 20, 15, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(50, 18, 'Rectifier Cabinet Outdoor', 'RectifierCOUTDR', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTSRectifierCOUTDR', 'MTS96a049', 20, 20, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(51, 18, 'ZTE Outdoor BTS', 'ZTEBTSOUTDR', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTSZTEBTSOUTDR', 'MTSb9c550', 20, 22, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(52, 18, 'UBBP Card', 'UBBPCRD', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTSUBBPCRD', 'MTS7bfc51', 20, 22, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(53, 18, 'APM30 Outdoor Cabinet', 'APM30CBNTOUTDR', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTSAPM30CBNTOUTDR', 'MTSbc6152', 20, 22, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(54, 18, 'EXT Rack Outdoor', 'EXTROODOR', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTSEXTROODOR', 'MTS766e53', 20, 22, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(55, 18, 'RRU Power Cable_Old', 'RRUPwrCBLOld', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Without-Tag', NULL, 'MTSb85654', 20, 23, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(56, 18, 'MRFU_1800', 'MRFU_1800', NULL, NULL, NULL, 52, '21,24', 'Supply,Service', 'Tag', 'MTSMRFU_1800', 'MTS119755', 20, 22, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(57, 18, 'BTS Power Cable Black', 'BTSPWRCBLBLK', NULL, NULL, NULL, 51, '21,24', 'Supply,Service', 'Without-Tag', NULL, 'MTSbdd756', 20, 23, '2022-02-15 11:57:01', '2022-02-15 11:57:01'),
(58, 18, 'BTS Power Cable Blue', 'BTSPWRCBLBLU', NULL, NULL, NULL, 51, '21,24', 'Supply,Service', 'Without-Tag', NULL, 'MTSd70357', 20, 23, '2022-02-15 11:57:01', '2022-02-15 11:57:01');

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `name`, `slug`, `parent_id`, `description`, `is_active`, `created_at`, `updated_at`) VALUES
(14, 'Feeder Cable', 'feedercable', NULL, NULL, NULL, '2022-01-30 09:29:37', '2022-01-30 09:29:37'),
(15, 'Optical Cable', 'opticalcable', NULL, NULL, NULL, '2022-01-30 09:29:37', '2022-01-30 09:29:37'),
(16, 'Feeder Connector', 'feederconnector', NULL, NULL, NULL, '2022-01-30 09:29:37', '2022-01-30 09:29:37'),
(17, 'Power Connector', 'powerconnector', NULL, NULL, NULL, '2022-01-30 09:29:37', '2022-01-30 09:29:37'),
(18, 'Jumper Cable', 'jumpercable', NULL, NULL, NULL, '2022-01-30 09:29:37', '2022-01-30 09:29:37'),
(19, 'Clamp/Kit', 'clamp/kit', NULL, NULL, NULL, '2022-01-30 09:29:37', '2022-01-30 09:29:37'),
(20, 'Power Accessories', 'poweraccessories', NULL, NULL, NULL, '2022-01-30 09:29:37', '2022-01-30 09:29:37'),
(21, 'Antenna', 'antenna', NULL, NULL, NULL, '2022-01-30 09:29:37', '2022-01-30 09:29:37'),
(22, 'BTS Accessories', 'btsaccessories', NULL, NULL, NULL, '2022-01-30 09:29:37', '2022-01-30 09:29:37'),
(23, 'Power Cable', 'powercable', NULL, NULL, NULL, '2022-01-30 09:29:37', '2022-01-30 09:29:37');

-- --------------------------------------------------------

--
-- Table structure for table `product_stocks`
--

CREATE TABLE `product_stocks` (
  `id` bigint UNSIGNED NOT NULL,
  `ppi_spi_id` bigint UNSIGNED NOT NULL,
  `action_format` enum('Ppi','Spi') COLLATE utf8mb4_unicode_ci NOT NULL,
  `ppi_spi_product_id` bigint UNSIGNED NOT NULL,
  `product_id` bigint UNSIGNED NOT NULL,
  `bundle_id` bigint UNSIGNED DEFAULT NULL,
  `barcode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `original_barcode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_unique_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stock_action` enum('In','Out') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stock_type` enum('Existing','New') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty` bigint UNSIGNED DEFAULT NULL,
  `entry_date` date DEFAULT NULL,
  `warehouse_id` bigint UNSIGNED NOT NULL,
  `action_performed_by` bigint UNSIGNED NOT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('Supply','Service') COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `code`, `type`, `customer`, `vendor`, `note`, `created_at`, `updated_at`) VALUES
(1, 'Project 1', 'project_1', 'Supply', 'BL', 'UDC', 'fdf', '2021-09-05 07:39:31', '2021-09-05 07:53:59');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('Global','General','Custom') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `code`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 'super_admin', 'Global', NULL, '2021-07-31 21:59:44'),
(3, 'User', 'user', 'General', NULL, '2021-09-21 10:58:48'),
(4, 'Subordinate Manager', 'subordinate_manager', 'Custom', NULL, '2022-03-03 04:45:34'),
(6, 'Warehouse Manager', 'warehouse_manager', 'Custom', '2021-07-28 11:06:30', '2022-03-04 19:57:54'),
(13, 'Boss', 'boss', 'Custom', '2021-09-24 14:55:58', '2022-03-05 05:59:08');

-- --------------------------------------------------------

--
-- Table structure for table `role_users`
--

CREATE TABLE `role_users` (
  `id` bigint UNSIGNED NOT NULL,
  `role_id` bigint UNSIGNED NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `warehouse_id` bigint UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_users`
--

INSERT INTO `role_users` (`id`, `role_id`, `user_id`, `warehouse_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL, NULL),
(54, 3, 17, NULL, '2021-07-30 14:00:08', '2021-07-30 14:00:08'),
(81, 3, 18, NULL, '2021-08-01 00:04:26', '2021-09-21 12:20:02'),
(121, 3, 23, NULL, '2021-09-24 23:09:51', '2021-09-24 23:09:51'),
(134, 4, 18, 24, NULL, NULL),
(135, 13, 23, 24, NULL, NULL),
(136, 3, 24, NULL, '2021-09-28 07:09:52', '2021-09-28 07:09:52'),
(140, 4, 18, 21, NULL, NULL),
(141, 13, 23, 21, NULL, NULL),
(142, 6, 24, 21, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `route_groups`
--

CREATE TABLE `route_groups` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `route_order` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `route_groups`
--

INSERT INTO `route_groups` (`id`, `name`, `code`, `route_order`, `created_at`, `updated_at`) VALUES
(105, 'Role', 'role', NULL, '2021-09-21 06:07:57', '2021-09-21 06:07:57'),
(106, 'User', 'user', NULL, '2021-09-21 06:07:57', '2021-09-21 06:07:57'),
(107, 'Routelist', 'routelist', NULL, '2021-09-21 06:07:57', '2021-09-21 06:07:57'),
(108, 'Warehouse', 'warehouse', NULL, '2021-09-21 06:07:57', '2021-09-21 06:07:57'),
(109, 'Product', 'product', NULL, '2021-09-21 06:07:58', '2021-09-21 06:07:58'),
(110, 'PPI', 'ppi', NULL, '2021-09-21 06:07:58', '2021-09-21 06:07:58'),
(111, 'Project', 'project', NULL, '2021-09-21 06:07:59', '2021-09-21 06:07:59'),
(112, 'Contact', 'contact', NULL, '2021-09-21 06:07:59', '2021-09-21 06:07:59'),
(113, 'Attribute', 'attribute', NULL, '2021-09-21 06:07:59', '2021-09-21 06:07:59'),
(114, 'PPI Action', 'ppiaction', NULL, '2021-09-26 05:22:38', '2021-09-26 05:22:38'),
(115, 'PPI Elements', 'ppielements', NULL, '2022-01-12 19:05:34', '2022-01-12 19:05:34'),
(116, 'SPI', 'spi', NULL, '2022-02-07 13:16:55', '2022-02-07 13:16:55'),
(117, 'SPI Action', 'spiaction', NULL, '2022-03-02 21:30:16', '2022-03-02 21:30:16');

-- --------------------------------------------------------

--
-- Table structure for table `route_lists`
--

CREATE TABLE `route_lists` (
  `id` bigint UNSIGNED NOT NULL,
  `route_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `route_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `route_parameter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route_group` bigint UNSIGNED DEFAULT NULL,
  `route_icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route_order` int DEFAULT NULL,
  `route_hash` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show_menu` enum('Yes','No') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_menu_id` int DEFAULT NULL,
  `dashboard_position` set('Left','Right','Top','Bottom') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show_for` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_show_as` enum('Yes','No') COLLATE utf8mb4_unicode_ci DEFAULT 'No',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `route_lists`
--

INSERT INTO `route_lists` (`id`, `route_title`, `route_name`, `route_parameter`, `route_description`, `route_group`, `route_icon`, `route_order`, `route_hash`, `show_menu`, `parent_menu_id`, `dashboard_position`, `show_for`, `is_show_as`, `created_at`, `updated_at`) VALUES
(416, 'Dashboard', 'admin_dashboard', '', 'Dashboard', NULL, 'fas fa-th', NULL, '$2y$10$BWp1fmxbyEyPfwVk3N3dyOCXAVI8oZeeMCgr8WNEFgRscUp/ZigZm', 'Yes', NULL, 'Left,Top', NULL, 'No', NULL, NULL),
(457, 'Manage Roles', 'role_index', '', 'Manage Roles', 105, 'far fa-folder', NULL, '$2y$10$HdMPeXsFPfBGuZbT5TyVouOPLWtuXjrpXGIjpbHNq4Kk1q.O6U03S', 'Yes', NULL, 'Left,Top', NULL, 'No', NULL, '2021-11-03 12:24:46'),
(458, 'Add', 'role_create', '', 'Add', 105, 'far fa-folder', NULL, '$2y$10$SKehFBVksrN.HSzJllCFce1Nj7c9kdx8.z5yMic2uL3AQoGMZiOSa', 'Yes', NULL, 'Left', NULL, 'No', NULL, NULL),
(459, 'Edit', 'role_edit', '', 'Edit', 105, 'far fa-folder', NULL, '$2y$10$hoCl6x/8frlVzF7LiSP27.JdS07cGnRgMRILtCJcvCVQFiuLiq1ou', NULL, NULL, NULL, NULL, 'No', NULL, NULL),
(460, 'Delete', 'role_destroy', '', 'Delete', 105, 'far fa-folder', NULL, '$2y$10$74Akuor8qoMbgiijZCL/C.qBQdpSfy8w/ay/hnDqNMQZja9DvCBg.', NULL, NULL, NULL, NULL, 'No', NULL, NULL),
(461, 'Manage Users', 'user_index', '', 'Manage Users', 106, 'far fa-folder', NULL, '$2y$10$9Ac/kVy/V3BAM120L7OEXeiTyIsC47e/A02NOMFUMtyBlzs2H.H0m', 'Yes', NULL, 'Left', NULL, 'No', NULL, NULL),
(462, 'Add', 'user_create', '', 'Add', 106, 'far fa-folder', NULL, '$2y$10$C6Dl7dXOzsH9AgT7wx4SZ.GFqQYYUZTCFxrzgLA4uTPgM74P64LO2', 'Yes', NULL, 'Left', NULL, 'No', NULL, NULL),
(463, 'Edit', 'user_edit', '', 'Edit', 106, 'far fa-folder', NULL, '$2y$10$chN4dsp2mdqxsmHyFpm50O/dzuMgzl8BJLx.DGHHEH2uHILJEuS2u', NULL, NULL, NULL, NULL, 'No', NULL, NULL),
(464, 'Delete', 'user_destroy', '', 'Delete', 106, 'far fa-folder', NULL, '$2y$10$Sbs9fkfl2tBsLlg5q96GWeb5k5JPmDCKln/kBOy452WLZx81aSbuq', NULL, NULL, NULL, NULL, 'No', NULL, NULL),
(465, 'Manage Route', 'routelist_index', '', 'Manage Route', 107, 'far fa-folder', NULL, '$2y$10$0EBxmz5vHwFdlHFOJ2b4bOiePVa3muqTbGjMcUZL2pFAKogws3Ni.', 'Yes', NULL, 'Left,Top', NULL, 'No', NULL, '2021-11-03 12:23:56'),
(466, 'Add', 'routelist_create', '', 'Add', 107, 'far fa-folder', NULL, '$2y$10$qaK.Wds9TuXsigog8V4qv.H7Z5A4AaqfV.sg8.NJubv0/bYqvLNfm', 'Yes', NULL, 'Left', NULL, 'No', NULL, NULL),
(467, 'Edit', 'routelist_edit', '', 'Edit', 107, 'far fa-folder', NULL, '$2y$10$ylxAP3YVlfCqfU5DBdA.d.CpGM/jZIeIx9EkZvahsny75vun9Edum', NULL, NULL, NULL, NULL, 'No', NULL, NULL),
(468, 'Delete', 'routelist_destroy', '', 'Delete', 107, 'far fa-folder', NULL, '$2y$10$GYJIQEarWOcRUVDU.65n4OKZKdJTCbeTuL/DP/NlnpQ2DgAbjD2uC', NULL, NULL, NULL, NULL, 'No', NULL, NULL),
(469, 'Manage Warehouse', 'warehouse_index', '', 'Manage Warehouse', 108, 'far fa-folder', NULL, '$2y$10$GgpWrmuGV.OkZgYQ15wnJuzuKjKKJwID1pOm8tw2b7QRgf0oKZyM.', 'Yes', NULL, 'Left,Top', NULL, 'No', NULL, NULL),
(470, 'Add', 'warehouse_create', '', 'Add', 108, 'far fa-folder', NULL, '$2y$10$9AeVL9r3ANJe9wHxstDb8uvjxEklM5eAzS/a4F1Q699foSIyyvfiC', 'Yes', NULL, 'Left', NULL, 'No', NULL, NULL),
(471, 'Edit', 'warehouse_edit', '', 'Edit', 108, 'far fa-folder', NULL, '$2y$10$Vk3UCq7rA5oKHx2mxE9TP./FdDUjIKoFq8p8T1PIKJ5dcupUGlgXO', NULL, NULL, NULL, NULL, 'No', NULL, NULL),
(472, 'Delete', 'warehouse_destroy', '', 'Delete', 108, 'far fa-folder', NULL, '$2y$10$KeYZ5HOdxLDgyWNm3mOEh.Dx.9rxGl4nIDMW6CLijNiODDuRKRbe.', NULL, NULL, NULL, NULL, 'No', NULL, NULL),
(473, 'View Warehouse', 'warehouse_single_index', '', 'View Warehouse', 108, 'far fa-folder', NULL, '$2y$10$UsHafCbIIb/ewiQGcOJATubT.qnv158nz6XAgQc/y5y/4NVydlNUK', NULL, NULL, NULL, NULL, 'No', NULL, NULL),
(474, 'Manage Products', 'product_index', '', 'Manage Products', 109, 'far fa-folder', NULL, '$2y$10$Q8MnQk03Smh69ZEzT61JyuSVjg8v5u1pyXze.NYXpp5xGqNi3v3pe', 'Yes', NULL, 'Left', 'Warehouse', 'Yes', NULL, NULL),
(475, 'Add Products', 'product_create', '', 'Add Products', 109, 'far fa-folder', NULL, '$2y$10$UfvKEQND8mKTgpO1Pipzpe9X8Ko/skDUyJ5cBPNl5zxG6ytCcadoi', 'Yes', NULL, 'Left', 'Warehouse', 'No', NULL, NULL),
(476, 'Edit', 'product_edit', '', 'Edit', 109, 'far fa-folder', NULL, '$2y$10$QhBE0fPGgOuneF2oken97e76ZIbSxMIeipQDRlpQPpb4MpvglNTuy', NULL, NULL, NULL, 'Warehouse', 'No', NULL, NULL),
(477, 'Delete', 'product_destroy', '', 'Delete', 109, 'far fa-folder', NULL, '$2y$10$qZKvWpYM.v3t410flsi8P.Yr.ouJb1xYoMwPKgoaCD3xASnu6M7iu', NULL, NULL, NULL, 'Warehouse', 'No', NULL, NULL),
(478, 'Manage Categories', 'product_category_index', '', 'Manage Categories', 109, 'far fa-folder', NULL, '$2y$10$EgpJlZFTxFWEHZDJW.OAVO2BGUJmnknR/.GhJwySNHOpfyDUYCQlG', 'Yes', NULL, 'Left', 'Warehouse', 'No', NULL, NULL),
(479, 'Edit Category', 'product_category_edit', '', 'Edit Category', 109, 'far fa-folder', NULL, '$2y$10$nPbQT.dNMKQp542ZfQ7/suRKIKwDdK.TXFX3y3o.kgXkRWavREqku', NULL, NULL, NULL, 'Warehouse', 'No', NULL, NULL),
(480, 'Delete Categories', 'product_category_destroy', '', 'Delete Categories', 109, 'far fa-folder', NULL, '$2y$10$TrDSw1jlwJW/SlWtvfg6E.teiBM8sLpWwQcmpIV43qDglv.Igb34e', NULL, NULL, NULL, 'Warehouse', 'No', NULL, NULL),
(481, 'Manage PPI', 'ppi_index', '', 'Manage PPI', 110, 'far fa-folder', NULL, '$2y$10$ZLFaRTG7JEHBJVgbHtyauOLtDVoB7.2.CGFQ7GF1P4v45IM0KVVp.', 'Yes', NULL, 'Left', 'Warehouse', 'Yes', NULL, NULL),
(482, 'Create PPI', 'ppi_create', '', 'Create PPI', 110, 'far fa-folder', NULL, '$2y$10$0pXW.YUmCKPIjea31S2GYuy8yN7WIwjWVBC/u420mNMlRDsFUoetu', 'Yes', NULL, 'Left', 'Warehouse', 'No', NULL, NULL),
(483, 'Edit', 'ppi_edit', '', 'Edit', 110, 'far fa-folder', NULL, '$2y$10$1Wuxs5Lp1qDe7xqZDtqdj.XUhVZR2XXR.PPVcm02.1HuVQPSGwwz2', NULL, NULL, NULL, 'Warehouse', 'No', NULL, NULL),
(484, 'Delete Product from Ppi', 'ppi_product_destroy', '', 'Delete Product from Ppi', 110, 'far fa-folder', NULL, '$2y$10$fM8Gr9HjwsM4eUHsO0mpDeB9SBMtexRLLwcmZ4kdjfIxr3793/foi', NULL, NULL, NULL, 'Warehouse', 'No', NULL, NULL),
(485, 'Manage Project', 'project_index', '', 'Manage Project', 111, 'far fa-folder', NULL, '$2y$10$bS05buCdzbIL9INUBe6IMujblkwA4z31cnDY0WClez4qhKZMkKkLm', 'Yes', NULL, 'Top', NULL, 'No', NULL, NULL),
(486, 'Add Project', 'project_create', '', 'Add Project', 111, 'far fa-folder', NULL, '$2y$10$mawOdkbstggcKSW3v/Z1t.7sn2f/mcZRFGthCX3mUrR7.3ilNWO2W', NULL, NULL, NULL, NULL, 'No', NULL, NULL),
(487, 'Edit Project', 'project_edit', '', 'Edit Project', 111, 'far fa-folder', NULL, '$2y$10$kvWMNkz.go0BKJjl2w8nfegnXgsuAry5S/YaW2qSrTDmluavJWNn2', NULL, NULL, NULL, NULL, 'No', NULL, NULL),
(488, 'Delete Project', 'project_destroy', '', 'Delete Project', 111, 'far fa-folder', NULL, '$2y$10$BqLgACF9Z2XrNIiI6pKC5.vy52oNkLx6qGnPj1MCEFRUiIq7Sd4vC', NULL, NULL, NULL, NULL, 'No', NULL, NULL),
(489, 'Manage Contact', 'contact_index', '', 'Manage Contact', 112, 'far fa-folder', NULL, '$2y$10$H.UXYGKIRW5eSxOxRNzeD.bBKZ.wZ6Yl7YGI18yNZ8k5LZdUCVzbu', 'Yes', NULL, 'Top', NULL, 'No', NULL, NULL),
(490, 'Add Contact', 'contact_create', '', 'Add Contact', 112, 'far fa-folder', NULL, '$2y$10$VhWvU03uYJTFWht.BNgGy.7LhYdPlkZBoRK15776DTZJ4SP0ZiW8a', NULL, NULL, NULL, NULL, 'No', NULL, NULL),
(491, 'Edit Contact', 'contact_edit', '', 'Edit Contact', 112, 'far fa-folder', NULL, '$2y$10$qLn7dr8O9/Yn/ZXICwnTiOkOkdLNJSlEl47.shaXYSdR2AL/weNem', NULL, NULL, NULL, NULL, 'No', NULL, NULL),
(492, 'Delete Contact', 'contact_destroy', '', 'Delete Contact', 112, 'far fa-folder', NULL, '$2y$10$t0ArO7bVMwCyofbSAbvvZePHjjigpswFpRk8stFxuH9FubU5WbpkW', NULL, NULL, NULL, NULL, 'No', NULL, NULL),
(493, 'Manage Unit', 'attribute_unit_index', 'Unit', 'Manage Unit', 113, 'far fa-folder', NULL, '$2y$10$je17fY7zSUuo/xxhUi.tjeq6LfBVXXqqebNxTqEZnKvTOZX1iHIdK', 'Yes', NULL, 'Top', NULL, 'No', NULL, NULL),
(494, 'Manage Brand', 'attribute_brand_index', 'Brand', 'Manage Brand', 113, 'far fa-folder', NULL, '$2y$10$KdxwJ53yhjeKZTBoUQt0YOmwNTSN5016nlCN/ktChrjJN0zZQoioW', 'Yes', NULL, 'Top', NULL, 'No', NULL, NULL),
(496, 'Edit Ppi Product Ppi', 'ppi_product_edit', '', 'Edit Ppi Product Ppi', 110, 'far fa-folder', NULL, '$2y$10$pH/zgiqXw6F7V9J2ag0IIeopk3HbUqE8kAgdMyXVL1AhR2RtUOBHW', NULL, NULL, NULL, 'Warehouse', 'No', NULL, NULL),
(497, 'Delete Set from Ppi', 'ppi_set_product_destroy', '', 'Delete Set from Ppi', 110, 'far fa-folder', NULL, '$2y$10$.ANwjUVXpalq/HgQ03lQVeg5nXeJ4foTgIAGmENGnCfVdh1FWboCS', NULL, NULL, NULL, 'Warehouse', 'No', NULL, NULL),
(498, 'Add Product to Ppi', 'ppi_product_add', '', 'Add Product to Ppi', 110, 'far fa-folder', NULL, '$2y$10$xfnFyu.RcIz9xua/J4LRiusUY8gj.uBdW.8HHvg4j1q1sT6ux560m', NULL, NULL, NULL, 'Warehouse', 'No', '2021-09-25 06:42:01', '2021-09-25 06:42:01'),
(499, 'Create Set Product to Ppi', 'ppi_set_product_add', '', 'Create Set Product to Ppi', 110, 'far fa-folder', NULL, '$2y$10$EV5qEtjQlwn/9REEFtXnm.UU11PDt.Lt8sXZD.PnmiVJomZNXXLi2', NULL, NULL, NULL, 'Warehouse', 'No', '2021-09-25 08:03:45', '2021-09-25 08:03:45'),
(501, 'Delete PPI', 'ppi_destroy', '', 'Delete PPI', 110, 'far fa-folder', NULL, '$2y$10$yacefTBNS19GoCoguRhV9OPOtDOmHO8WA8KN9/i6iJ8hbktai3suW', NULL, NULL, NULL, 'Warehouse', 'No', '2021-09-25 15:02:42', '2021-09-25 15:02:42'),
(514, 'Sent to Boss', 'ppi_sent_to_boss_action', '', 'Sent to Boss', 114, 'far fa-folder', NULL, '$2y$10$pfm1NZsKVDmdk0GmGAcjMO/6V4FTrB3SDBykfANI7nfgs9aaL5EGa', NULL, NULL, NULL, 'Warehouse', 'No', '2021-11-02 08:58:22', '2021-11-02 08:58:22'),
(516, 'Sent to Warehouse manager', 'ppi_sent_to_wh_manager_action', '', 'Sent to Warehouse manager', 114, 'far fa-folder', NULL, '$2y$10$ClnNgWwyJtB18UVDeAjM7.I4Ml5ZjJtqEjEnueKG/isHpg1qxclNy', NULL, NULL, NULL, 'Warehouse', 'No', '2021-11-02 08:58:22', '2021-11-02 08:58:22'),
(518, 'Dispute by Warehouse Manager', 'ppi_dispute_by_wh_manager_action', '', 'Dispute by Warehouse Manager', 114, 'far fa-folder', NULL, '$2y$10$mpvZTA9G6yB07oSDMnQ04OFUHACgZdzt67quX0F1li4Za.mNW5qDm', NULL, NULL, NULL, 'Warehouse', 'No', '2021-11-02 09:37:07', '2021-11-02 09:37:07'),
(519, 'Global Settings', 'admin_global_settings', '', 'Global Settings', NULL, 'fas fa-cog', NULL, '$2y$10$zx4SdNghKLffmhT5VEKRi.TTBrDQOIE0kP13tQQ4mSNrYH0BiSBtO', 'Yes', NULL, 'Left,Top', NULL, 'No', '2021-11-09 09:18:56', '2021-11-09 09:18:56'),
(521, 'Ppi resent to Warehouse Manager', 'ppi_resent_to_wh_manager_action', '', 'Ppi resent to Warehouse Manager', 114, 'far fa-folder', NULL, '$2y$10$09OfwUtJHT37XVkfAIBXA.vtw/27taxivvBFg7W9lIVwvYmFAfVOa', NULL, NULL, NULL, 'Warehouse', 'No', '2021-11-10 12:58:51', '2021-11-10 12:58:51'),
(524, 'Ppi Product Information correction by boss', 'ppi_product_info_correction_by_boss_action', '', 'Ppi Product Information correction by boss', 114, 'far fa-folder', NULL, '$2y$10$kxwxuleCYM/z9YEj5AA06.j0iTQMCT8BKweEXp6BGjdOJMP7iJFxW', NULL, NULL, NULL, 'Warehouse', 'No', '2021-11-11 12:42:34', '2021-11-11 12:42:34'),
(525, 'Ready to physical validation', 'ppi_ready_to_physical_validation_action', '', 'Ready to physical validation', 114, 'far fa-folder', NULL, '$2y$10$0A6RKnPtCD4cu0eeDYOL4e3XBrct/WoXTp7mW5nXad4WO2Z1ElJAi', NULL, NULL, NULL, 'Warehouse', 'No', '2021-11-15 13:09:10', '2021-11-15 13:09:10'),
(526, 'Barcode Page', 'ppi_get_line_item', '', 'Barcode Page', 110, 'far fa-folder', NULL, '$2y$10$LtIDgRc4yTDgG5NcqvpQF.c1274DvfZ1ZhEZYp1DRBQpgv/tg/6Mu', NULL, NULL, NULL, 'Warehouse', 'No', '2021-11-17 02:53:04', '2021-11-17 02:53:04'),
(528, 'Delete Product from Set', 'ppi_product_destroy_from_set', '', 'Delete Product from Set', 110, 'far fa-folder', NULL, '$2y$10$ilZbwV.xeUgcCQMiYGTrb.G3IPSdJDjRVULmn7aluo9AjoMNuXbbC', NULL, NULL, NULL, 'Warehouse', 'No', '2021-11-29 09:53:33', '2021-11-29 09:53:33'),
(531, 'Ppi Product Price Show', 'ppi_product_price_show_element', '', 'Ppi Product Price Show', 115, 'far fa-folder', NULL, '$2y$10$ob1c3dfHxT/6XztBFPTzZ.7FvY5ey88ZEZZY16batZkRy.IwMCT/m', NULL, NULL, NULL, 'Warehouse', 'No', '2022-01-12 19:20:31', '2022-01-12 19:20:31'),
(582, 'Manage SPI', 'spi_index', '', 'Manage SPI', 116, 'far fa-folder', NULL, '$2y$10$omBZ0tS4QBPZ64x/oqIhpu9wue5OZ6TOm2IwiLI3jcIKKenK0654.', 'Yes', NULL, 'Left', 'Warehouse', 'Yes', '2022-02-07 16:56:14', '2022-02-07 17:00:43'),
(583, 'Create SPI', 'spi_create', '', 'Create SPI', 116, 'far fa-folder', NULL, '$2y$10$4vatDTQKpEx.jbLQOcePoeSTxCLi/KOEpsHdF38z3V2wB89VlScO2', 'Yes', NULL, 'Left', 'Warehouse', 'No', '2022-02-07 16:56:14', '2022-02-07 16:56:14'),
(584, 'Edit', 'spi_edit', '', 'Edit', 116, 'far fa-folder', NULL, '$2y$10$xw61sJrBP/rOsqhygH1K3.d.jUaUU8NJ2pm4n/plh4q8VECbs6Uii', NULL, NULL, NULL, 'Warehouse', 'No', '2022-02-07 16:56:14', '2022-02-07 16:56:14'),
(585, 'Delete SPI', 'spi_destroy', '', 'Delete SPI', 116, 'far fa-folder', NULL, '$2y$10$pXpPS.ZFL7Tr4gA3SI5cc.Sg4RJTxFZHwTCLdy.HiqPDdInuqyQS2', NULL, NULL, NULL, 'Warehouse', 'No', '2022-02-07 16:56:15', '2022-02-07 16:56:15'),
(586, 'Manage Contact Type', 'attribute_contact type_index', 'Contact Type', 'Manage Contact Type', 113, 'far fa-folder', NULL, '$2y$10$nCg.p342hoekJFHguNhAi.eyYSkxUZH0chPiOPsSyZevslC1UCvj6', 'Yes', NULL, 'Top', NULL, 'No', '2022-02-08 05:15:31', '2022-02-08 05:15:31'),
(587, 'Add Product to Spi', 'spi_product_add', '', 'Add Product to Spi', 116, 'far fa-folder', NULL, '$2y$10$B61C8Ml0vObP5QYh55EPEe87FMnAs05B2vgetHjIdNZNneFvNgcmC', NULL, NULL, '', 'Warehouse', 'No', '2022-02-10 16:40:10', '2022-02-10 17:01:12'),
(588, 'Edit Spi Product', 'spi_product_edit', '', 'Edit Spi Product', 116, 'far fa-folder', NULL, '$2y$10$SrzKMUviCnXkxKjdHZYFbe9dpVRD3gyC7Q7K9M6GtGEV.i66LX70G', NULL, NULL, '', 'Warehouse', 'No', '2022-02-10 16:40:10', '2022-02-10 17:02:02'),
(589, 'Delete Product from Spi', 'spi_product_destroy', '', 'Delete Product from Spi', 116, 'far fa-folder', NULL, '$2y$10$ZrlYHR9qgcp4oRCnHcvnmeyD2BMA4Lxu.dDlg8OC2YuK3Y2H8hjF.', NULL, NULL, NULL, 'Warehouse', 'No', '2022-02-10 16:40:10', '2022-02-10 16:40:10'),
(590, 'Sent to Boss', 'spi_sent_to_boss_action', '', 'Sent to Boss', 117, 'far fa-folder', NULL, '$2y$10$eELuBBeurrWhRTQ2FX3V5OuY53Rj6XNTk./pplnLQmabTSjKPQwYm', NULL, NULL, NULL, 'Warehouse', 'No', '2022-03-02 21:30:16', '2022-03-02 21:30:16'),
(591, 'Sent to Warehouse manager', 'spi_sent_to_wh_manager_action', '', 'Sent to Warehouse manager', 117, 'far fa-folder', NULL, '$2y$10$6qV3BgU2zNdjm22ydntmYufbbU7Fbw0lxLNcQ62Go9CrTLShDbei6', NULL, NULL, NULL, 'Warehouse', 'No', '2022-03-03 05:40:35', '2022-03-03 05:40:35'),
(592, 'Physical Validate Page', 'spi_get_line_item', '', 'Physical Validate Page', 116, 'far fa-folder', NULL, '$2y$10$sP2th.59TjXReyP9OdEYK.mULjFTL5W4sR5VdIgxYLFzrZf8KVBA2', NULL, NULL, NULL, 'Warehouse', 'No', '2022-03-04 19:05:34', '2022-03-04 19:05:34'),
(593, 'Ready to physical validation', 'spi_ready_to_physical_validation_action', '', 'Ready to physical validation', 117, 'far fa-folder', NULL, '$2y$10$VNQZxER402ZbVoKUppfp9.ER2ZI/Brd1tcpfx/f3al5Wt78MqbJNm', NULL, NULL, NULL, 'Warehouse', 'No', '2022-03-04 19:57:42', '2022-03-04 19:57:42'),
(594, 'Dispute by Warehouse Manager', 'spi_dispute_by_wh_manager_action', '', 'Dispute by Warehouse Manager', 117, 'far fa-folder', NULL, '$2y$10$V7xzFe7hr9Z5qkhn/NzgZ.npxhVdVqHLQ.SdgzYoJ4zjMPNSlzsOi', NULL, NULL, NULL, 'Warehouse', 'No', '2022-03-04 19:57:43', '2022-03-04 19:57:43'),
(595, 'Spi Product Information correction by boss', 'spi_product_info_correction_by_boss_action', '', 'Spi Product Information correction by boss', 117, 'far fa-folder', NULL, '$2y$10$iTYUCXwSboTx23PTI6ERUuq3yj57AsFdZAZTctUxRC.vzR7wKX02.', NULL, NULL, NULL, 'Warehouse', 'No', '2022-03-04 19:57:43', '2022-03-04 19:57:43'),
(596, 'Spi resent to Warehouse Manager', 'spi_resent_to_wh_manager_action', '', 'Spi resent to Warehouse Manager', 117, 'far fa-folder', NULL, '$2y$10$.UHpff3NB3bkIR2KH3inDOEFc8rIxfcIk92cHddscg8t2QaVquzeW', NULL, NULL, NULL, 'Warehouse', 'No', '2022-03-04 19:57:43', '2022-03-04 19:57:43');

-- --------------------------------------------------------

--
-- Table structure for table `route_list_roles`
--

CREATE TABLE `route_list_roles` (
  `id` bigint UNSIGNED NOT NULL,
  `role_id` bigint UNSIGNED NOT NULL,
  `route_id` bigint UNSIGNED NOT NULL,
  `show_as` enum('All','User','Permission') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `route_list_roles`
--

INSERT INTO `route_list_roles` (`id`, `role_id`, `route_id`, `show_as`, `created_at`, `updated_at`) VALUES
(119, 3, 416, NULL, '2021-09-21 10:58:48', '2021-09-21 10:58:48'),
(120, 3, 469, NULL, '2021-09-21 10:58:48', '2021-09-21 10:58:48'),
(121, 3, 473, NULL, '2021-09-21 10:58:48', '2021-09-21 10:58:48'),
(1102, 4, 416, NULL, '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1103, 4, 469, NULL, '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1104, 4, 474, 'All', '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1105, 4, 475, NULL, '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1106, 4, 476, NULL, '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1107, 4, 477, NULL, '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1108, 4, 478, NULL, '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1109, 4, 479, NULL, '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1110, 4, 480, NULL, '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1111, 4, 481, 'User', '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1112, 4, 482, NULL, '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1113, 4, 483, NULL, '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1114, 4, 484, NULL, '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1115, 4, 496, NULL, '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1116, 4, 497, NULL, '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1117, 4, 498, NULL, '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1118, 4, 499, NULL, '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1119, 4, 501, NULL, '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1120, 4, 528, NULL, '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1121, 4, 514, NULL, '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1122, 4, 516, NULL, '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1123, 4, 531, NULL, '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1124, 4, 582, 'User', '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1125, 4, 583, NULL, '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1126, 4, 584, NULL, '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1127, 4, 585, NULL, '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1128, 4, 587, NULL, '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1129, 4, 588, NULL, '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1130, 4, 589, NULL, '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1131, 4, 590, NULL, '2022-03-03 04:45:34', '2022-03-03 04:45:34'),
(1184, 6, 416, NULL, '2022-03-04 19:57:54', '2022-03-04 19:57:54'),
(1185, 6, 469, NULL, '2022-03-04 19:57:54', '2022-03-04 19:57:54'),
(1186, 6, 473, NULL, '2022-03-04 19:57:54', '2022-03-04 19:57:54'),
(1187, 6, 481, 'Permission', '2022-03-04 19:57:54', '2022-03-04 19:57:54'),
(1188, 6, 483, NULL, '2022-03-04 19:57:54', '2022-03-04 19:57:54'),
(1189, 6, 526, NULL, '2022-03-04 19:57:54', '2022-03-04 19:57:54'),
(1190, 6, 518, NULL, '2022-03-04 19:57:54', '2022-03-04 19:57:54'),
(1191, 6, 525, NULL, '2022-03-04 19:57:54', '2022-03-04 19:57:54'),
(1192, 6, 582, NULL, '2022-03-04 19:57:54', '2022-03-04 19:57:54'),
(1193, 6, 583, NULL, '2022-03-04 19:57:54', '2022-03-04 19:57:54'),
(1194, 6, 584, NULL, '2022-03-04 19:57:54', '2022-03-04 19:57:54'),
(1195, 6, 585, NULL, '2022-03-04 19:57:54', '2022-03-04 19:57:54'),
(1196, 6, 592, NULL, '2022-03-04 19:57:54', '2022-03-04 19:57:54'),
(1197, 6, 593, NULL, '2022-03-04 19:57:54', '2022-03-04 19:57:54'),
(1222, 13, 416, NULL, '2022-03-05 05:59:08', '2022-03-05 05:59:08'),
(1223, 13, 469, NULL, '2022-03-05 05:59:08', '2022-03-05 05:59:08'),
(1224, 13, 474, 'All', '2022-03-05 05:59:08', '2022-03-05 05:59:08'),
(1225, 13, 481, 'Permission', '2022-03-05 05:59:08', '2022-03-05 05:59:08'),
(1226, 13, 483, NULL, '2022-03-05 05:59:08', '2022-03-05 05:59:08'),
(1227, 13, 484, NULL, '2022-03-05 05:59:08', '2022-03-05 05:59:08'),
(1228, 13, 496, NULL, '2022-03-05 05:59:08', '2022-03-05 05:59:08'),
(1229, 13, 497, NULL, '2022-03-05 05:59:08', '2022-03-05 05:59:08'),
(1230, 13, 498, NULL, '2022-03-05 05:59:08', '2022-03-05 05:59:08'),
(1231, 13, 499, NULL, '2022-03-05 05:59:08', '2022-03-05 05:59:08'),
(1232, 13, 526, NULL, '2022-03-05 05:59:08', '2022-03-05 05:59:08'),
(1233, 13, 528, NULL, '2022-03-05 05:59:08', '2022-03-05 05:59:08'),
(1234, 13, 516, NULL, '2022-03-05 05:59:08', '2022-03-05 05:59:08'),
(1235, 13, 521, NULL, '2022-03-05 05:59:08', '2022-03-05 05:59:08'),
(1236, 13, 524, NULL, '2022-03-05 05:59:08', '2022-03-05 05:59:08'),
(1237, 13, 531, NULL, '2022-03-05 05:59:08', '2022-03-05 05:59:08'),
(1238, 13, 582, 'Permission', '2022-03-05 05:59:08', '2022-03-05 05:59:08'),
(1239, 13, 584, NULL, '2022-03-05 05:59:08', '2022-03-05 05:59:08'),
(1240, 13, 587, NULL, '2022-03-05 05:59:08', '2022-03-05 05:59:08'),
(1241, 13, 588, NULL, '2022-03-05 05:59:08', '2022-03-05 05:59:08'),
(1242, 13, 589, NULL, '2022-03-05 05:59:08', '2022-03-05 05:59:08'),
(1243, 13, 592, NULL, '2022-03-05 05:59:08', '2022-03-05 05:59:08'),
(1244, 13, 591, NULL, '2022-03-05 05:59:08', '2022-03-05 05:59:08'),
(1245, 13, 595, NULL, '2022-03-05 05:59:08', '2022-03-05 05:59:08'),
(1246, 13, 596, NULL, '2022-03-05 05:59:08', '2022-03-05 05:59:08');

-- --------------------------------------------------------

--
-- Table structure for table `spi_products`
--

CREATE TABLE `spi_products` (
  `id` bigint UNSIGNED NOT NULL,
  `spi_id` bigint UNSIGNED NOT NULL,
  `warehouse_id` bigint UNSIGNED NOT NULL,
  `product_id` bigint UNSIGNED NOT NULL,
  `ppi_product_id` bigint UNSIGNED NOT NULL,
  `ppi_id` bigint UNSIGNED NOT NULL,
  `bundle_id` bigint UNSIGNED DEFAULT NULL,
  `qty` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit_price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_performed_by` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marital_status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `father` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mother` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergency_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postcode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `join_date` date DEFAULT NULL,
  `employee_status` enum('Enroll','Terminated','Long Leave','Left Job','On Hold') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `employee_no`, `username`, `phone`, `gender`, `marital_status`, `father`, `mother`, `emergency_phone`, `company`, `department`, `address`, `postcode`, `district`, `email_verified_at`, `birthday`, `join_date`, `employee_status`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Md. Khalakuzzaman Khan', 'info@tritiyo.com', NULL, NULL, '01680139540', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, 'Block L, Road No. 8, South Banasree', '1703', 'Dhaka', NULL, NULL, NULL, 'Enroll', '$2y$10$.qnxQEbp.n7VSaKWMP0sMe34737/Phlq/eO3iawnJqywftukdxmuq', 'nrmsoSEBxg9q87hBhDD5acf4iQ2rVUG5THqwu9QE5MES6ZMzf3e8OC7w8qLt', '2021-07-14 11:54:50', '2021-07-28 08:16:20'),
(17, 'Noushad Nipun', 'nipun@tritiyo.com', NULL, NULL, '01677618199', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, 'Ghatail', '1980', 'Tangail', NULL, NULL, NULL, NULL, '$2y$10$6wisrDzoaM3GU6ceZUXmlu/yjk6ianySYJWrh16MA7fFqCPumJZnu', NULL, '2021-07-30 14:00:08', '2021-07-30 14:00:08'),
(18, 'Anowarul Haque', 'anowar@mtsbd.net', NULL, NULL, '0', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$GsBD2qR4JaNfYOqq8oL9reuHxJ09kpFu/dhjbvQlEjc1RZSzuMqFe', NULL, '2021-07-30 15:03:53', '2021-09-21 12:20:02'),
(23, 'Zabidur Rahman', 'zabid@mtsbd.net', NULL, NULL, '0171', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, 'n/a', 'n/a', 'Dhaka', NULL, NULL, NULL, NULL, '$2y$10$r.GOG2ijgKY2KTUVZssMkO2vTtYT6svMb0bdztCfFMypaT2FOkm/W', NULL, '2021-09-24 23:09:51', '2021-09-24 23:09:51'),
(24, 'Rabbi', 'rabbi@mtsbd.net', 'u001', NULL, '019', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, 'Dhaka', '1215', 'Dhaka', NULL, NULL, NULL, NULL, '$2y$10$xSlnLvHdv8PLDb3URacn/.c1zJFsqOfAd7qnJ/cebAmJOpt0WHHPO', NULL, '2021-09-28 07:09:52', '2021-09-28 07:09:52');

-- --------------------------------------------------------

--
-- Table structure for table `warehouses`
--

CREATE TABLE `warehouses` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `warehouses`
--

INSERT INTO `warehouses` (`id`, `name`, `location`, `code`, `phone`, `email`, `created_at`, `updated_at`) VALUES
(21, 'Warehouse 1', 'DHK', 'warehouse_1_af9b', '01680139540', 'info@tritiyo.com', '2021-07-30 15:04:50', '2021-11-16 07:12:31'),
(24, 'Warehouse 2', 'Dhaka', 'warehouse_2_2031', '019', 'w2@mail.com', '2021-09-21 06:09:51', '2021-09-26 05:46:41'),
(25, 'Warehouse 3', 'Dhaka', 'warehouse_3_2f24', '32', 'info@mtsbd.net', '2022-03-01 08:31:53', '2022-03-01 08:31:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attribute_values`
--
ALTER TABLE `attribute_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contacts_phone_unique` (`phone`);

--
-- Indexes for table `global_settings`
--
ALTER TABLE `global_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `global_settings_meta_name_unique` (`meta_name`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `ppi_bundle_products`
--
ALTER TABLE `ppi_bundle_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ppi_bundle_products_ppi_id_foreign` (`ppi_id`),
  ADD KEY `ppi_bundle_products_ppi_product_id_foreign` (`ppi_product_id`),
  ADD KEY `ppi_bundle_products_warehouse_id_foreign` (`warehouse_id`),
  ADD KEY `ppi_bundle_products_action_performed_by_foreign` (`action_performed_by`);

--
-- Indexes for table `ppi_products`
--
ALTER TABLE `ppi_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ppi_products_ppi_id_foreign` (`ppi_id`),
  ADD KEY `ppi_products_product_id_foreign` (`product_id`),
  ADD KEY `ppi_products_warehouse_id_foreign` (`warehouse_id`),
  ADD KEY `ppi_products_action_performed_by_foreign` (`action_performed_by`);

--
-- Indexes for table `ppi_set_products`
--
ALTER TABLE `ppi_set_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ppi_set_products_ppi_id_foreign` (`ppi_id`),
  ADD KEY `ppi_set_products_warehouse_id_foreign` (`warehouse_id`),
  ADD KEY `ppi_set_products_action_performed_by_foreign` (`action_performed_by`);

--
-- Indexes for table `ppi_spis`
--
ALTER TABLE `ppi_spis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ppis_warehouse_id_foreign` (`warehouse_id`),
  ADD KEY `ppis_action_performed_by_foreign` (`action_performed_by`);

--
-- Indexes for table `ppi_spi_disputes`
--
ALTER TABLE `ppi_spi_disputes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ppi_spi_disputes_ppi_product_id_foreign` (`ppi_spi_product_id`),
  ADD KEY `ppi_spi_disputes_ppi_spi_status_id_foreign` (`ppi_spi_status_id`),
  ADD KEY `ppi_spi_disputes_warehouse_id_foreign` (`warehouse_id`),
  ADD KEY `ppi_spi_disputes_action_performed_by_foreign` (`action_performed_by`);

--
-- Indexes for table `ppi_spi_sources`
--
ALTER TABLE `ppi_spi_sources`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ppi_spi_sources_warehouse_id_foreign` (`warehouse_id`),
  ADD KEY `ppi_spi_sources_ppi_spi_id_foreign` (`ppi_spi_id`);

--
-- Indexes for table `ppi_spi_statuses`
--
ALTER TABLE `ppi_spi_statuses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ppi_spi_statuses_warehouse_id_foreign` (`warehouse_id`),
  ADD KEY `ppi_spi_statuses_action_performed_by_foreign` (`action_performed_by`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_code_unique` (`code`),
  ADD UNIQUE KEY `products_slug_unique` (`slug`),
  ADD UNIQUE KEY `unique_key` (`unique_key`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_categories_slug_unique` (`slug`);

--
-- Indexes for table `product_stocks`
--
ALTER TABLE `product_stocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_stocks_product_id_foreign` (`product_id`),
  ADD KEY `product_stocks_warehouse_id_foreign` (`warehouse_id`),
  ADD KEY `product_stocks_action_performed_by_foreign` (`action_performed_by`),
  ADD KEY `product_stock_ppi_id` (`ppi_spi_id`),
  ADD KEY `product_stock_bundle` (`bundle_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `projects_code_unique` (`code`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_users`
--
ALTER TABLE `role_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_users_role_id_foreign` (`role_id`),
  ADD KEY `role_users_user_id_foreign` (`user_id`),
  ADD KEY `role_users_warehouse_id_foreign` (`warehouse_id`);

--
-- Indexes for table `route_groups`
--
ALTER TABLE `route_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `route_lists`
--
ALTER TABLE `route_lists`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `route_lists_route_name_unique` (`route_name`),
  ADD KEY `route_lists_route_group_foreign` (`route_group`);

--
-- Indexes for table `route_list_roles`
--
ALTER TABLE `route_list_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `route_list_roles_role_id_foreign` (`role_id`),
  ADD KEY `route_list_roles_route_id_foreign` (`route_id`);

--
-- Indexes for table `spi_products`
--
ALTER TABLE `spi_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `spi_products_spi_id_foreign` (`spi_id`),
  ADD KEY `spi_products_ppi_id_foreign` (`ppi_id`),
  ADD KEY `spi_products_ppi_product_id_foreign` (`ppi_product_id`),
  ADD KEY `spi_products_product_id_foreign` (`product_id`),
  ADD KEY `spi_products_warehouse_id_foreign` (`warehouse_id`),
  ADD KEY `spi_products_action_performed_by_foreign` (`action_performed_by`),
  ADD KEY `spi_products_bundle_foreign` (`bundle_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `employee_no` (`employee_no`);

--
-- Indexes for table `warehouses`
--
ALTER TABLE `warehouses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attribute_values`
--
ALTER TABLE `attribute_values`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `global_settings`
--
ALTER TABLE `global_settings`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `ppi_bundle_products`
--
ALTER TABLE `ppi_bundle_products`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `ppi_products`
--
ALTER TABLE `ppi_products`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `ppi_set_products`
--
ALTER TABLE `ppi_set_products`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ppi_spis`
--
ALTER TABLE `ppi_spis`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `ppi_spi_disputes`
--
ALTER TABLE `ppi_spi_disputes`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `ppi_spi_sources`
--
ALTER TABLE `ppi_spi_sources`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `ppi_spi_statuses`
--
ALTER TABLE `ppi_spi_statuses`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=198;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `product_stocks`
--
ALTER TABLE `product_stocks`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=267;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `role_users`
--
ALTER TABLE `role_users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;

--
-- AUTO_INCREMENT for table `route_groups`
--
ALTER TABLE `route_groups`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;

--
-- AUTO_INCREMENT for table `route_lists`
--
ALTER TABLE `route_lists`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=597;

--
-- AUTO_INCREMENT for table `route_list_roles`
--
ALTER TABLE `route_list_roles`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1247;

--
-- AUTO_INCREMENT for table `spi_products`
--
ALTER TABLE `spi_products`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `warehouses`
--
ALTER TABLE `warehouses`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ppi_bundle_products`
--
ALTER TABLE `ppi_bundle_products`
  ADD CONSTRAINT `ppi_bundle_products_action_performed_by_foreign` FOREIGN KEY (`action_performed_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ppi_bundle_products_ppi_id_foreign` FOREIGN KEY (`ppi_id`) REFERENCES `ppi_spis` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ppi_bundle_products_ppi_product_id_foreign` FOREIGN KEY (`ppi_product_id`) REFERENCES `ppi_products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ppi_bundle_products_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ppi_products`
--
ALTER TABLE `ppi_products`
  ADD CONSTRAINT `ppi_products_action_performed_by_foreign` FOREIGN KEY (`action_performed_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ppi_products_ppi_id_foreign` FOREIGN KEY (`ppi_id`) REFERENCES `ppi_spis` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ppi_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ppi_products_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ppi_set_products`
--
ALTER TABLE `ppi_set_products`
  ADD CONSTRAINT `ppi_set_products_action_performed_by_foreign` FOREIGN KEY (`action_performed_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ppi_set_products_ppi_id_foreign` FOREIGN KEY (`ppi_id`) REFERENCES `ppi_spis` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ppi_set_products_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ppi_spis`
--
ALTER TABLE `ppi_spis`
  ADD CONSTRAINT `ppis_action_performed_by_foreign` FOREIGN KEY (`action_performed_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ppis_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ppi_spi_disputes`
--
ALTER TABLE `ppi_spi_disputes`
  ADD CONSTRAINT `ppi_spi_disputes_action_performed_by_foreign` FOREIGN KEY (`action_performed_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ppi_spi_disputes_ppi_spi_status_id_foreign` FOREIGN KEY (`ppi_spi_status_id`) REFERENCES `ppi_spi_statuses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ppi_spi_disputes_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ppi_spi_sources`
--
ALTER TABLE `ppi_spi_sources`
  ADD CONSTRAINT `ppi_spi_sources_ppi_spi_id_foreign` FOREIGN KEY (`ppi_spi_id`) REFERENCES `ppi_spis` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ppi_spi_sources_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ppi_spi_statuses`
--
ALTER TABLE `ppi_spi_statuses`
  ADD CONSTRAINT `ppi_spi_statuses_action_performed_by_foreign` FOREIGN KEY (`action_performed_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ppi_spi_statuses_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_stocks`
--
ALTER TABLE `product_stocks`
  ADD CONSTRAINT `product_stock_bundle` FOREIGN KEY (`bundle_id`) REFERENCES `ppi_bundle_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_stock_ppi_id` FOREIGN KEY (`ppi_spi_id`) REFERENCES `ppi_spis` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_stocks_action_performed_by_foreign` FOREIGN KEY (`action_performed_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_stocks_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_stocks_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_users`
--
ALTER TABLE `role_users`
  ADD CONSTRAINT `role_users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_users_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `route_lists`
--
ALTER TABLE `route_lists`
  ADD CONSTRAINT `route_lists_route_group_foreign` FOREIGN KEY (`route_group`) REFERENCES `route_groups` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `route_list_roles`
--
ALTER TABLE `route_list_roles`
  ADD CONSTRAINT `route_list_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `route_list_roles_route_id_foreign` FOREIGN KEY (`route_id`) REFERENCES `route_lists` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `spi_products`
--
ALTER TABLE `spi_products`
  ADD CONSTRAINT `spi_products_action_performed_by_foreign` FOREIGN KEY (`action_performed_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `spi_products_bundle_foreign` FOREIGN KEY (`bundle_id`) REFERENCES `ppi_bundle_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `spi_products_ppi_id_foreign` FOREIGN KEY (`ppi_id`) REFERENCES `ppi_spis` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `spi_products_ppi_product_id_foreign` FOREIGN KEY (`ppi_product_id`) REFERENCES `ppi_products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `spi_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `spi_products_spi_id_foreign` FOREIGN KEY (`spi_id`) REFERENCES `ppi_spis` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `spi_products_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
