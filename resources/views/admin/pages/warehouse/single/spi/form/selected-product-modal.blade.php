<div class="selected-product-modal" id="reload_modal">
    <?php
    // $checkStock = $Model('ProductStock')::
    //                                    leftjoin('ppi_spis', 'ppi_spis.id', 'product_stocks.ppi_spi_id')
    //                                    ->select('ppi_spis.*', 'product_stocks.*')
    //                                    ->where('product_stocks.action_format', 'Ppi')
    //                                    ->where('product_stocks.product_id', $product_id)
    //                                    ->get();
    $checkStock = function ($ppi_spi_type) use ($Model, $product_id, $warehouse_id) {
        return $Model('ProductStock')::with('ppiSpi', 'source', 'ppiProduct', 'productInfo')
            ->where('action_format', 'Ppi')
            ->where('product_id', $product_id)
            ->where('warehouse_id', $warehouse_id)
            ->whereHas('ppiSpi', function ($q) use ($ppi_spi_type) {
                $q->where('ppi_spi_type', $ppi_spi_type);
            })
            //->groupBy('ppi_spi_id')
            ->groupBy('ppi_spi_product_id')
            ->get();
    };

    //echo $product_id;
    //echo request()->get('warehouse_id');
    //dd($checkStock('Service'));
    //    dd($checkStock($browse));
    //    echo $browsePpi;
    //
    //dump($checkStock($browse));
    ?>

    <div class="modal_sub_header_wrap">
        <div class="warehouse_list bg-warning bg-gradient">
            @php
                $warehouses = $Model('Warehouse')::get();
            @endphp
            @foreach($warehouses as $warehouse)
                <button class="btn btn_browse btn-sm rounded-0 {{$warehouse_id == $warehouse->id ? 'btn-primary' : ''}}"
                        data-warehouse_id="{{ $warehouse->id ?? NULL }}" data-browse="{{$browse}}"
                        data-row_id=" {{$row_id}}" data-warehouse_code={{$warehouse->code}}
                    data-product_id="{{$product_id}}">
                    {{ $warehouse->name ?? NULL }}
                </button>
            @endforeach
        </div>

        <div class="button_set d-flex align-items-center">
            <div class="flex-fill text-start text-secondary">
                <strong title="product_id: {{$product_id}}">{{$Model('Product')::getColumn($product_id, 'name')}}</strong> |
                <small class="font-11 fw-bold">Product Code:{{$Model('Product')::getColumn($product_id, 'code')}}</small>
            </div>
            <div class="flex-fill text-end">
                <ul class="nav nav-tabs d-inline-flex">
                    <li class="nav-item">
                        <a class="nav-link btn_browse d-inline-block py-1 {{$browse == 'Supply' ? 'active' : ''}}"
                           type="button" data-browse="Supply" data-row_id=" {{$row_id}}"
                           data-warehouse_id="{{$warehouse_id}}" data-warehouse_code={{$warehouse->code}}
                            data-product_id="{{$product_id}}">Supply</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn_browse d-inline-block py-1 {{$browse == 'Service' ? 'active' : ''}}"
                           type="button" data-browse="Service" data-row_id=" {{$row_id}}"
                           data-warehouse_id="{{$warehouse_id}}" data-warehouse_code={{$warehouse->code}}
                            data-product_id="{{$product_id}}">Service</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="mt-2">
            <input type="text" name="" id="searchInput" value="" class="form-control form-control-sm d-block w-100"
                   placeholder="Search by PPI ID, Bundle Size, Project Name.....">
        </div>
    </div>

    <div class="row mx-0 myTable">

        @foreach($checkStock($browse) as $key =>  $item)
{{--                    @dump($item)--}}
            @php
                $checkBundle =  $Model('PpiBundleProduct')::where('ppi_id', $item->ppi_spi_id)
                                    ->where('ppi_product_id', $item->ppi_spi_product_id)
                                    ->where('product_id', $item->product_id)
                                    ->get();
                //dump($checkBundle)
            @endphp
            <?php
                $produnctTemplate = function($bundleKey = null, $bundle = null) use ($key, $item, $Model, $warehouse_id) {
                $makeKey = $bundleKey > 0 ? 'b'.$bundleKey : 'bw'.$key;
                //dump($makeKey);
            ?>

                @php
                if($bundle){
                     $stockIn = $Model('ProductStock')::checkStock($item->product_id, 'Ppi', 'In', [
                         'warehouse_id' => $warehouse_id,
                         'bundle_id' => $bundle->id,
                         'ppi_spi_id' => $item->ppi_spi_id,
                         'ppi_spi_product_id' => $item->ppi_spi_product_id,
                         ]);
                    $stockOut = $Model('ProductStock')::checkStock($item->product_id, 'Spi', 'Out', ['warehouse_id' => $warehouse_id, 'bundle_id' => $bundle->id]);
                    //$stockInhand = $stockIn - $stockOut;
                    $stockInhand = $stockIn;
                    $unit_price = $bundle->bundle_price;
                    $input_Qty = $stockInhand == 0 ? '0' : $bundle->bundle_size;
                    $inputDisabled = 'disabled';
                    $bundle_id = $bundle->id;
                    //dump($item->product_id);
                } else {
                     $stockIn = $Model('ProductStock')::checkStock($item->product_id, 'Ppi', 'In', [
                        'warehouse_id' => $warehouse_id,
                        'ppi_spi_id' => $item->ppi_spi_id,
                        'ppi_spi_product_id' => $item->ppi_spi_product_id,
                        ]);
                    $stockOut = $Model('ProductStock')::checkStock($item->product_id, 'Spi', 'Out', ['warehouse_id' => $warehouse_id]);
                    //$stockInhand = $stockIn - $stockOut;
                    $stockInhand = $stockIn;
                    $unit_price = $item->ppiProduct->unit_price;
                     $input_Qty = $stockInhand == 0 ? 0 : 1;
                     $inputDisabled = null;
                     $bundle_id = null;
                }


            $checkWaitListForStockOut = $Model('SpiProduct')::leftjoin('ppi_spi_statuses as sts', 'spi_products.id', 'sts.ppi_spi_product_id')
                                       ->select('spi_products.qty')
                                       ->where('spi_products.from_warehouse', $item->warehouse_id)
                                       ->where('spi_products.product_id', $item->product_id)
                                       ->where('spi_products.ppi_product_id', $item->ppi_spi_product_id)
                                       ->where('spi_products.bundle_id', $bundle_id)
                                       ->whereNotIn('sts.code', ['spi_product_out_from_stock'])
                                       ->groupBy('ppi_spi_product_id')
                                       ->get()
                                       ->sum('qty');
            //dump($item);
            //dump($checkWaitListForStockOut);
            //dump($stockInhand);
            //dump($stockOut);
            $checkWaitListForStockOut = !empty($checkWaitListForStockOut) ? $checkWaitListForStockOut: 0;
            $stockInhand = $stockInhand-$checkWaitListForStockOut;
            $input_Qty = $stockInhand == 0 ? 0 : $input_Qty;
            @endphp
                @if($stockInhand > 0)
                    <div class="col-lg-12 mb-2 font-11 bw-1 border-gray p-2 shadow-sm tr selectedRowId{{$makeKey}}">
                        <div class="">
                            <span class="td">

                                <span title="ppi_product_id: {{$item->ppiProduct->id}} product_id: {{$item->productInfo->id}}">
                                    <strong>{{$item->action_format}} ID:</strong> <span
                                        class="tdshow">{{$item->ppi_spi_id}} </span> <span class="text-dark fw-bold">.</span>
                                </span>
                                <span>
                                    <strong>{{$item->action_format}} Type:</strong> {{$item->ppiSpi->ppi_spi_type}} <span
                                        class="text-dark fw-bold">.</span>
                                </span>
                                <span>
                                    <strong>Project:</strong> <span class="tdshow">{{$item->ppiSpi->project}}</span> <span
                                        class="text-dark fw-bold">.</span>
                                </span>
                                <span>
                                    <strong>Stock in hand:</strong> {{$stockInhand}} <span class="text-dark fw-bold">.</span>
                                </span>
                                <span>
                                    <strong>Product State:</strong>  <span
                                        class="tdshow">  {{$item->ppiProduct->product_state }} </span> <span
                                        class="text-dark fw-bold">.</span>
                                </span>
                                <span>
                                    <strong>Health Status:</strong> <span
                                        class="tdshow">{{$item->ppiProduct->health_status }} </span> <span
                                        class="text-dark fw-bold">.</span>
                                </span>
                                <span>
                                    <strong>Unit Price:</strong> {{$unit_price}}
                                </span>
                                <br>
                                <!-- If Bundle -->
                                @if($bundle)
                                    <div class="product_bundle_wrap" title="bundle_id: {{$bundle_id}}">
                                         <strong>Bundle Size: </strong>
                                        <span class="tdshow"> {!!  $bundle->bundle_size !!}  </span>
                                    </div> <!-- End Bundle -->
                                @endif
                                <div class="crumbswrapper d-inline-block">
                                    <div class="crumbs mx-1 my-0 mt-1" id="source_breadcrumb">
                                        <?php foreach($item->source as $source): ?>
                                            <div class="innerwrap">
                                                <span class="innerItem font-11 ps-2 pe-1 tdshow">
                                                    <span>{{$source->source_type}}:</span> {{$source->who_source}}
                                                </span>
                                            </div>
                                        <?php endforeach;?>
                                    </div>
                                </div>
                            </span>

                            <div class="float-end text-end">
                                <label for="">Qty</label>
                                <input {{$inputDisabled}} type="number" id="sing_qty"
                                       class="p-0 d-inline-block border-gray bw-1 h-20 text-center w-25"
                                       data-qty="{{$input_Qty}}"
                                       name=""
                                       value="{{$input_Qty}}"
                                       data-max="{{ $stockInhand }}"
                                       data-min="0">
                                <span class="text-danger">MTR</span>
                                @if($input_Qty > 0)
                                    <button type="button" class="btn-dt bg-primary text-white bw-1 bg-primary border-primary"
                                            id="sing_qty_add"
                                            data-bundle_id="{{$bundle_id}}"
                                            data-ppi_product_id="{{$item->ppi_spi_product_id}}"
                                            data-ppi_id="{{$item->ppi_spi_id}}"
                                            data-selected_row="Id{{$makeKey}}"
                                            data-product_qty="{{$input_Qty}}">
                                        Add
                                    </button>
                                @endif
                            </div>
                        </div>
                    </div>
                 @endif
            <?php } ?>

            @if(count($checkBundle) > 0)
                @foreach($checkBundle as $bundleKey => $bundle)
                    {!! $produnctTemplate($bundle->id, $bundle) !!}
                @endforeach
            @else
                {!! $produnctTemplate() !!}
            @endif
        @endforeach
    </div>


</div>
<script>
    let inputSingQty = $("input#sing_qty");
    $(inputSingQty).keyup(function () {
        let thisVal = $(this).val()
        let maxVal = $(this).data('max')
        let minVal = $(this).data('min')
        // alert(maxVal)
        if (thisVal > maxVal) {
            alert('You have exceeded the maximum quantity')
            $(this).val(minVal)
        } else if (thisVal < minVal) {
            $(this).val(minVal)
        } else {

        }

    })

    $('button#sing_qty_add').click(function () {
        let selectedRow = $(this).data('selected_row');
        let selectedRowCls = $('.selectedRow' + selectedRow + ' input#sing_qty');
        let qty = selectedRowCls.val();
        //alert(selectedRow);
        //let qty = $(this).data('product_qty');
        $(".colgroup.prb{{$row_id}} input#qty").val(qty)
        let ppiId = $(this).data('ppi_id');
        let ppiProductId = $(this).data('ppi_product_id');
        let fromWarehouse = "{{$warehouse_id}}"
        let bundleID = $(this).data('bundle_id');
        // alert(ppiId)
        let html = '<input type="hidden" value="' + ppiId + '" name="product[{{$row_id}}][ppi_id]" />';
        html += '<input type="hidden" value="' + ppiProductId + '" name="product[{{$row_id}}][ppi_product_id]" />';
        html += '<input type="hidden" value="' + fromWarehouse + '" name="product[{{$row_id}}][from_warehouse]" />';
        if (bundleID) {
            html += '<input type="hidden" value="' + bundleID + '" name="product[{{$row_id}}][bundle_id]" />';
        }
        $('.ppiInformation{{$row_id}} .ppi_id_append').html(html)
        $('.selectedProductInfoOpenModal').modal('hide');
        toastr.success('Product Qty Added');
    })


    /** Button Browse Supply/Service Action */
    $('.selectedProductInfoOpenModal').one('click', '.btn_browse', function (e) {
        e.preventDefault();
        let dataBrowse = $(this).data('browse');
        let rowId = $(this).data('row_id');
        let productId = $(this).data('product_id');
        let warehouseCode = $(this).data('warehouse_code') ?? "{{request()->get('warehouse_code')}}";
        let thisUrl = '{{route("spi_selected_product_details_info", '+warehouseCode+')}}';
        let warehouseId = $(this).data('warehouse_id') ?? "{{request()->get('warehouse_id')}}";

        $(this).off('click');
        // $('.selectedProductInfoOpenModal').empty()
        // alert(dataBrowse)
        $(" #reload_modal").empty();
        $.ajax({
            url: thisUrl,
            method: 'GET',
            data: {
                {{--'_token': "{!! csrf_token() !!}",--}}
                'browse': dataBrowse,
                'row_id': rowId,
                'product_id': productId,
                'warehouse_id': warehouseId,
            },
            success: function (response) {
                // $('.selectedProductInfoOpenModal #reload_modal').html(response).fadeIn(1000)
                $('#reload_modal').empty().append(response)
            }
        })
    })


    /**
     * Modal Header And Footer Design and Button , Search Box add
     */
    $('.selectedProductInfoOpenModal .modal-header').addClass('d-block py-1 pb-auto').html($('.modal_sub_header_wrap').html())
    $('.selectedProductInfoOpenModal .modal-footer').addClass('py-1 pb-auto')
    $('.modal_sub_header_wrap').empty();

    /** Search Functionality */
    $(document).ready(function () {
        $('#searchInput').keyup(function () {

            // Search text
            var text = $(this).val();

            // Hide all conten  t class element
            $('.tr').hide();

            // Search and show
            // $('.tr:contains("'+text+'")').show();
            // $('.tdshow:contains("'+text+'")').show();
            $('.tr .tdshow').each(function () {
                // console.log($(this).text())
                let chkUpperCase = $(this).text().toUpperCase().indexOf("" + text + "");
                let chkLowerCase = $(this).text().toLowerCase().indexOf("" + text + "");
                if (chkUpperCase != -1 || chkLowerCase != -1) {
                    $('.tdshow:contains("' + text + '")').closest('.tr').show();
                }
            })
        });
    });

    $.expr[":"].contains = $.expr.createPseudo(function (arg) {
        return function (elem) {
            return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
        };
    });

</script>


<style>
    /* Chrome, Safari, Edge, Opera */
    input#sing_qty::-webkit-outer-spin-button,
    input#sing_qty::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input#sing_qty {
        -moz-appearance: textfield;
    }
</style>


