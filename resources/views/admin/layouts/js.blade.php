<!-- bootstrap bundle js -->
{{-- <script src="{{ $publicDir }}/js/app.js "></script> --}}
<script src="{{ $publicDir }}/assets/js/jquery-1.12.4.min.js "></script>
<script src="{{ $publicDir }}/assets/js/bootstrap.bundle.min.js "></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js "></script>
<script src="{{ $publicDir }}/assets/js/jquery-ui.js "></script>

<script src="{{ $publicDir }}/assets/js/custom.js "></script>

<script src="{{ $publicDir }}/assets/js/printThis.js"></script>


{{--<script>--}}
{{--    $('body').append('<div style="" id="loadingDiv"><div class="loader">Loading...</div></div>');--}}
{{--    $(window).on('load', function(){--}}
{{--        setTimeout(removeLoader, 2000); //wait for page load PLUS two seconds.--}}
{{--    });--}}
{{--    function removeLoader(){--}}
{{--        $( "#loadingDiv" ).fadeOut(500, function() {--}}
{{--            // fadeOut complete. Remove the loading div--}}
{{--            $( "#loadin gDiv" ).remove(); //makes page more lightweight--}}
{{--        });--}}
{{--    }--}}
{{--</script>--}}

{{--<style>--}}
{{--    .loader,--}}
{{--    .loader:after {--}}
{{--        border-radius: 50%;--}}
{{--        width: 10em;--}}
{{--        height: 10em;--}}
{{--    }--}}
{{--    .loader {--}}
{{--        margin: auto;--}}
{{--        top: 50%;--}}
{{--        left: 0%;--}}
{{--        transform: translate(-50%, -50%);--}}
{{--        font-size: 10px;--}}
{{--        position: relative;--}}
{{--        text-indent: -9999em;--}}
{{--        border-top: 1.1em solid rgba(255, 255, 255, 0.2);--}}
{{--        border-right: 1.1em solid rgba(255, 255, 255, 0.2);--}}
{{--        border-bottom: 1.1em solid rgba(255, 255, 255, 0.2);--}}
{{--        border-left: 1.1em solid #2330b0;--}}
{{--        -webkit-transform: translateZ(0);--}}
{{--        -ms-transform: translateZ(0);--}}
{{--        transform: translateZ(0);--}}
{{--        -webkit-animation: load8 1.1s infinite linear;--}}
{{--        animation: load8 1.1s infinite linear;--}}
{{--    }--}}
{{--    @-webkit-keyframes load8 {--}}
{{--        0% {--}}
{{--            -webkit-transform: rotate(0deg);--}}
{{--            transform: rotate(0deg);--}}
{{--        }--}}
{{--        100% {--}}
{{--            -webkit-transform: rotate(360deg);--}}
{{--            transform: rotate(360deg);--}}
{{--        }--}}
{{--    }--}}
{{--    @keyframes load8 {--}}
{{--        0% {--}}
{{--            -webkit-transform: rotate(0deg);--}}
{{--            transform: rotate(0deg);--}}
{{--        }--}}
{{--        100% {--}}
{{--            -webkit-transform: rotate(360deg);--}}
{{--            transform: rotate(360deg);--}}
{{--        }--}}
{{--    }--}}
{{--    #loadingDiv {--}}
{{--        position:fixed;--}}
{{--        top:0;--}}
{{--        left:0;--}}
{{--        width:100%;--}}
{{--        height:100%;--}}
{{--        background-color: #f6f6f6;--}}
{{--    }--}}
{{--</style>--}}
