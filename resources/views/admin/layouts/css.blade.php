<!-- bootstrap css -->
<link rel="stylesheet" href="{{ $publicDir }}/assets/css/bootstrap.min.css">

<!-- font awesome -->
<link rel="stylesheet" href="{{ $publicDir }}/assets/css/all.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"/>
<!-- icons 8 -->
<link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">

<!-- select2 -->
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

<!-- jquery ui css -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/blitzer/jquery-ui.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<!-- blue css/color css -->
<link rel="stylesheet" href="{{ $publicDir }}/assets/css/blue.css">

<!-- style css -->
<link rel="stylesheet" href="{{ $publicDir }}/assets/css/style.css?<?php echo rand(0,9999) ?>">
<link rel="stylesheet" href="{{ $publicDir }}/assets/css/form.css?<?php echo rand(0,9999) ?>">
<!-- responsive css -->
<link rel="stylesheet" href="{{ $publicDir }}/assets/css/responsive.css?<?php echo rand(0,9999) ?>">
<link rel="stylesheet" href="{{ $publicDir }}/assets/css/button.css?<?php echo rand(0,9999) ?>">
<link rel="stylesheet" href="{{ $publicDir }}/assets/css/custom.css?<?php echo rand(0,9999) ?>">
<script src="{{ $publicDir }}/assets/js/jquery-1.12.4.min.js "></script>

